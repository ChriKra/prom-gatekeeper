package de.chrikra.query;

import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


import org.apache.commons.collections4.CollectionUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import lombok.AllArgsConstructor;

public class ParametrizedPromQLParserTest {
	@ParameterizedTest(name = "{0}")
	@MethodSource("createTests")
	void test(String query, Matcher<QueryElement> matcher) throws IOException, ParseException {
		QueryElement queryElement = new PromQLParser().parse(query);
		assertThat(queryElement, matcher);
	}

//	@ParameterizedTest(name = "{0}")
//	@MethodSource("createTests")
//	void testToString(String query, Matcher<QueryElement> matcher, String expectedOutput) throws IOException, ParseException {
//		QueryElement queryElement = parse(query);
//		assertThat(queryElement.toString(), equalTo(expectedOutput));
//	}

	private static Stream<Arguments> createTests() {
		return Stream.of(Arguments.of("1", new NLMatcher(1.0d), "1.0"), Arguments.of("+1", new NLMatcher(1.0d), "1.0"),
				Arguments.of("-1", new NLMatcher(-1.0d), "1.0"), Arguments.of(".5", new NLMatcher(0.5d), "0.5"),
				Arguments.of("5.", new NLMatcher(5.0d), "5.0"),
				Arguments.of("123.4567", new NLMatcher(123.4567d), "123.4567"),
				Arguments.of("5e-3", new NLMatcher(0.005d), "0.005"),
				Arguments.of("5e3", new NLMatcher(5000d), "5000.0"), Arguments.of("0xc", new NLMatcher(12d), "12.0"),
				Arguments.of("0755", new NLMatcher(493d), "493.0"),
				Arguments.of("+5.5e-3", new NLMatcher(0.0055d), "0.0055"),
				Arguments.of("-0755", new NLMatcher(-493d), "-493.0"),
				Arguments.of("0755", new NLMatcher(493d), "493.0"), Arguments.of("+Inf", new NLMatcher(true), "+Inf"),
				Arguments.of("-Inf", new NLMatcher(false), "-Inf"),
				Arguments.of("1 + 1", new BEMatcher(new NLMatcher(1.0d), "+", new NLMatcher(1.0f)), "1.0 + 1.0"),
				Arguments.of("1 - 1", new BEMatcher(new NLMatcher(1.0d), "-", new NLMatcher(1.0f)), "1.0 - 1.0"),
				Arguments.of("1-1", new BEMatcher(new NLMatcher(1.0d), "-", new NLMatcher(1.0f)), "1.0 - 1.0"),
				Arguments.of("1 % 1", new BEMatcher(new NLMatcher(1.0d), "%", new NLMatcher(1.0f)), "1.0 % 1.0"),
				Arguments.of("1 * 1", new BEMatcher(new NLMatcher(1.0d), "*", new NLMatcher(1.0f)), "1.0 * 1.0"),
				Arguments.of("1 / 1", new BEMatcher(new NLMatcher(1.0d), "/", new NLMatcher(1.0f)), "1.0 / 1.0"),
				Arguments.of("1 == bool 1", new BEMatcher(new NLMatcher(1.0d), "==", new NLMatcher(1.0f), true),
						"1.0 == bool 1.0"),
				Arguments.of("1 != bool 1", new BEMatcher(new NLMatcher(1.0d), "!=", new NLMatcher(1.0f), true),
						"1.0 != bool 1.0"),
				Arguments.of("1 > bool 1", new BEMatcher(new NLMatcher(1.0d), ">", new NLMatcher(1.0f), true),
						"1.0 > bool 1.0"),
				Arguments.of("1 >= bool 1", new BEMatcher(new NLMatcher(1.0d), ">=", new NLMatcher(1.0f), true),
						"1.0 >= bool 1.0"),
				Arguments.of("1 < bool 1", new BEMatcher(new NLMatcher(1.0d), "<", new NLMatcher(1.0f), true),
						"1.0 < bool 1.0"),
				Arguments.of("1 <= bool 1", new BEMatcher(new NLMatcher(1.0d), "<=", new NLMatcher(1.0f), true),
						"1.0 <= bool 1.0"),
				Arguments.of("-1^2", new BEMatcher(new NLMatcher(-1.0d), "^", new NLMatcher(2.0f)), "-1.0 ^ 2.0"),
				Arguments.of("-1*2", new BEMatcher(new NLMatcher(-1.0d), "*", new NLMatcher(2.0f)), "-1.0 * 2.0"),
				Arguments.of("-1+2", new BEMatcher(new NLMatcher(-1.0d), "+", new NLMatcher(2.0f)), "-1.0 + 2.0"),
				Arguments.of("-1^-2", new BEMatcher(new NLMatcher(-1.0d), "^", new NLMatcher(-2.0f)), "-1.0 ^ -2.0"),
				Arguments.of("+1 + -2 * 1",
						new BEMatcher(new NLMatcher(1d), "+",
								new BEMatcher(new NLMatcher(-2.0d), "*", new NLMatcher(1.0f))),
						"1.0 + -2.0 * 1.0"),

				Arguments.of("1 + 2/(3*1)",
						new BEMatcher(new NLMatcher(1d), "+",
								new BEMatcher(new NLMatcher(2.0d), "/",
										new ParenExpressionMatcher(
												new BEMatcher(new NLMatcher(3.0d), "*", new NLMatcher(1.0d))))),
						"1.0 + 2.0 / (3.0 * 1.0)"),
				Arguments.of("1 < bool 2 - 1 * 2",
						new BEMatcher(new NLMatcher(1.0d), "<",
								new BEMatcher(new NLMatcher(2.0d), "-",
										new BEMatcher(new NLMatcher(1.0d), "*", new NLMatcher(2.0d))),
								true),
						"1.0 < bool 2.0 - 1.0 * 2.0"),
				Arguments.of("foo * bar", new BEMatcher(new VSMatcher("foo"), "*", new VSMatcher("bar")), "foo * bar"),
				Arguments.of("foo * sum", new BEMatcher(new VSMatcher("foo"), "*", new VSMatcher("sum")), "foo * sum"),
				Arguments.of("foo == 1", new BEMatcher(new VSMatcher("foo"), "==", new NLMatcher(1.0d))),
				Arguments.of("foo == bool 1", new BEMatcher(new VSMatcher("foo"), "==", new NLMatcher(1.0d), true)),
				Arguments.of("2.5 / bar", new BEMatcher(new NLMatcher(2.5d), "/", new VSMatcher("bar"))),
				Arguments.of("foo and bar", new BEMatcher(new VSMatcher("foo"), "and", new VSMatcher("bar"))),
				Arguments.of("foo or bar", new BEMatcher(new VSMatcher("foo"), "or", new VSMatcher("bar"))),
				Arguments.of("foo + bar or bla and blub",
						new BEMatcher(new BEMatcher(new VSMatcher("foo"), "+", new VSMatcher("bar")), "or",
								new BEMatcher(new VSMatcher("bla"), "and", new VSMatcher("blub")))),
				Arguments.of("-some_metric", new UEMatcher(new VSMatcher("some_metric"), "-")),
				Arguments.of("+some_metric", new UEMatcher(new VSMatcher("some_metric"), "+")),
				Arguments.of("foo and bar unless baz or qux",
						new BEMatcher(new BEMatcher(new BEMatcher(new VSMatcher("foo"), "and", new VSMatcher("bar")),
								"unless", new VSMatcher("baz")), "or", new VSMatcher("qux"))),
				Arguments.of("foo", new VSMatcher("foo")), Arguments.of("min", new VSMatcher("min")),
				Arguments.of("foo offset 5m", new OEMatcher(new VSMatcher("foo"), "5m")),
				Arguments.of("foo:bar{a=\"bc\"}", new VSMatcher("foo:bar").addLabel("a", "bc")),
				Arguments.of("foo{NaN='bc'}", new VSMatcher("foo").addLabel("NaN", "bc")),
				Arguments.of("foo{bar='}'}", new VSMatcher("foo").addLabel("bar", "}")),
				Arguments.of("foo{a=\"b\", foo!=\"bar\", test=~\"test\", bar!~\"baz\"}",
						new VSMatcher("foo").addLabel("a", "b").addLabel("foo", "!=", "bar")
								.addLabel("test", "=~", "test").addLabel("bar", "!~", "baz")),
				Arguments.of("test[5s]", new MSMatcher(new VSMatcher("test"), "5s")),
				Arguments.of("test[5m]", new MSMatcher(new VSMatcher("test"), "5m")),
				Arguments.of("test[5h] OFFSET 5m", new OEMatcher(new MSMatcher(new VSMatcher("test"), "5h"), "5m")),
				Arguments.of("test[5d] OFFSET 10s", new OEMatcher(new MSMatcher(new VSMatcher("test"), "5d"), "10s")),
				Arguments.of("test[5w] OFFSET 2w", new OEMatcher(new MSMatcher(new VSMatcher("test"), "5w"), "2w")),
				Arguments.of("test{a=\"b\"}[5y] OFFSET 3d",
						new OEMatcher(new MSMatcher(new VSMatcher("test").addLabel("a", "b"), "5y"), "3d")),
				Arguments.of("sum by (foo)(some_metric)",
						new AEMatcher("sum", "by").addBodyMatcher(new VSMatcher("some_metric")).addGrouping("foo")),
				Arguments.of("avg by (foo)(some_metric)",
						new AEMatcher("avg", "by").addBodyMatcher(new VSMatcher("some_metric")).addGrouping("foo")),
				Arguments.of("max by (foo)(some_metric)",
						new AEMatcher("max", "by").addBodyMatcher(new VSMatcher("some_metric")).addGrouping("foo")),
				Arguments.of("sum without (foo) (some_metric)",
						new AEMatcher("sum", "without").addBodyMatcher(new VSMatcher("some_metric"))
								.addGrouping("foo")),
				Arguments.of("sum (some_metric) without (foo)",
						new AEMatcher("sum", "without").addBodyMatcher(new VSMatcher("some_metric"))
								.addGrouping("foo")),
				Arguments.of("stddev(some_metric)",
						new AEMatcher("stddev").addBodyMatcher(new VSMatcher("some_metric"))),
				Arguments.of("stdvar by (foo)(some_metric)",
						new AEMatcher("stdvar", "by").addGrouping("foo").addBodyMatcher(new VSMatcher("some_metric"))),
				Arguments.of("sum by ()(some_metric)",
						new AEMatcher("sum", "by").addBodyMatcher(new VSMatcher("some_metric"))),
				Arguments.of("sum by (foo,bar,)(some_metric)",
						new AEMatcher("sum", "by").addBodyMatcher(new VSMatcher("some_metric")).addGrouping("foo")
								.addGrouping("bar")),
				Arguments.of("sum by (foo,)(some_metric)",
						new AEMatcher("sum", "by").addBodyMatcher(new VSMatcher("some_metric")).addGrouping("foo")),
				Arguments.of("topk(5, some_metric)",
						new AEMatcher("topk").addBodyMatcher(new NLMatcher(5d))
								.addBodyMatcher(new VSMatcher("some_metric"))),
				Arguments.of("count_values(\"value\", some_metric)",
						new AEMatcher("count_values").addBodyMatcher(new SLMatcher("value"))
								.addBodyMatcher(new VSMatcher("some_metric"))),
				Arguments.of("sum without(and, by, avg, count, alert, annotations)(some_metric)",
						new AEMatcher("sum", "without").addBodyMatcher(new VSMatcher("some_metric")).addGrouping("and")
								.addGrouping("by").addGrouping("avg").addGrouping("count").addGrouping("alert")
								.addGrouping("annotations")),
				Arguments.of("time()", new FCMatcher("time")),
				Arguments.of("floor(some_metric{foo!=\"bar\"})",
						new FCMatcher("floor")
								.addBodyMatcher(new VSMatcher("some_metric").addLabel("foo", "!=", "bar"))),
				Arguments.of("rate(some_metric[5m])",
						new FCMatcher("rate").addBodyMatcher(new MSMatcher(new VSMatcher("some_metric"), "5m"))),
				Arguments.of("round(some_metric)", new FCMatcher("round").addBodyMatcher(new VSMatcher("some_metric"))),
				Arguments.of("round(some_metric, 5)",
						new FCMatcher("round").addBodyMatcher(new VSMatcher("some_metric"))
								.addBodyMatcher(new NLMatcher(5d))),
				Arguments.of("sum(sum)", new AEMatcher("sum").addBodyMatcher(new VSMatcher("sum"))),
				Arguments.of("a + sum", new BEMatcher(new VSMatcher("a"), "+", new VSMatcher("sum"))),
				Arguments.of("\"double-quoted string \\\" with escaped quote\"",
						new SLMatcher("double-quoted string \\\" with escaped quote")),
				Arguments.of("'single-quoted string \\' with escaped quote'",
						new SLMatcher("single-quoted string \\' with escaped quote")),
				Arguments.of("`backtick-quoted string`", new SLMatcher("backtick-quoted string")),
				Arguments.of("\"\\a\\b\\f\\n\\r\\t\\v\\\\\\\" - \\xFF\\377\\u1234\\U00010111\\U0001011111☺\"",
						new SLMatcher("\\a\\b\\f\\n\\r\\t\\v\\\\\\\" - \\xFF\\377\\u1234\\U00010111\\U0001011111☺")),
				Arguments.of("'\\a\\b\\f\\n\\r\\t\\v\\\\\\' - \\xFF\\377\\u1234\\U00010111\\U0001011111☺'",
						new SLMatcher("\\a\\b\\f\\n\\r\\t\\v\\\\\\' - \\xFF\\377\\u1234\\U00010111\\U0001011111☺")),
				Arguments.of("foo{bar=\"baz\"}[10m:6s]",
						new SQMatcher(new VSMatcher("foo").addLabel("bar", "baz"), "10m", "6s")),
				Arguments.of("foo[10m:]", new SQMatcher(new VSMatcher("foo"), "10m", null)),
				Arguments.of("min_over_time(rate(foo{bar=\"baz\"}[2s])[5m:5s])",
						new FCMatcher("min_over_time").addBodyMatcher(new SQMatcher(
								new FCMatcher("rate").addBodyMatcher(
										new MSMatcher(new VSMatcher("foo").addLabel("bar", "baz"), "2s")),
								"5m", "5s"))),
				Arguments
						.of("min_over_time(rate(foo{bar=\"baz\"}[2s])[5m:] offset 4m)[4m:3s]",
								new SQMatcher(new FCMatcher("min_over_time").addBodyMatcher(new OEMatcher(new SQMatcher(
										new FCMatcher("rate").addBodyMatcher(
												new MSMatcher(new VSMatcher("foo").addLabel("bar", "baz"), "2s")),
										"5m", null), "4m")), "4m", "3s")),
				Arguments.of("sum without(and, by, avg, count, alert, annotations)(some_metric) [30m:10s]",
						new SQMatcher(new AEMatcher("sum", "without").addGrouping("and").addGrouping("by")
								.addGrouping("avg").addGrouping("count").addGrouping("alert").addGrouping("annotations")
								.addBodyMatcher(new VSMatcher("some_metric")), "30m", "10s")),
				Arguments.of("some_metric OFFSET 1m [10m:5s]", new SQMatcher(
						new OEMatcher(new VSMatcher("some_metric"), "1m"), "10m", "5s")),
				Arguments
						.of("(foo + bar{nm=\"val\"})[5m:]",
								new SQMatcher(
										new ParenExpressionMatcher(new BEMatcher(new VSMatcher("foo"), "+",
												new VSMatcher("bar").addLabel("nm", "val"))),
										"5m", null)),
				Arguments.of("(foo + bar{nm=\"val\"})[5m:] offset 10m",
						new OEMatcher(new SQMatcher(new ParenExpressionMatcher(
								new BEMatcher(new VSMatcher("foo"), "+", new VSMatcher("bar").addLabel("nm", "val"))),
								"5m", null), "10m")),
				Arguments.of("'singleQuotedString'", new SLMatcher("singleQuotedString")),
				Arguments.of("\"doubleQuotedString\"", new SLMatcher("doubleQuotedString"))

		);
	}

	@AllArgsConstructor
	private static class OEMatcher extends BaseMatcher<QueryElement> {
		private Matcher<QueryElement> matcher;
		private String offset;

		@Override
		public boolean matches(Object actual) {
			if (!(actual instanceof OffsetExpression)) {
				return false;
			}
			OffsetExpression oe = (OffsetExpression) actual;
			boolean result = offset.equals(oe.getOffset());
			result &= matcher.matches(oe.getExpression());
			return result;
		}

		@Override
		public void describeTo(Description description) {

		}

	}

	@AllArgsConstructor
	private static class MSMatcher extends BaseMatcher<QueryElement> {

		private Matcher<QueryElement> matcher;
		private String duration;

		@Override
		public boolean matches(Object actual) {
			if (!(actual instanceof MatrixSelector)) {
				return false;
			}
			MatrixSelector vs = (MatrixSelector) actual;
			boolean result = matcher.matches(vs.getExpression());
			result &= duration.equals(vs.getDuration());
			return result;
		}

		@Override
		public void describeTo(Description description) {
			// TODO Auto-generated method stub

		}

	}

	private static class FCMatcher extends BaseMatcher<QueryElement> {
		private String functionName;
		private List<BaseMatcher<QueryElement>> bodyMatchers;

		public FCMatcher(String functionName) {
			super();
			this.functionName = functionName;
		}

		public FCMatcher addBodyMatcher(BaseMatcher<QueryElement> matcher) {
			if (bodyMatchers == null) {
				bodyMatchers = new ArrayList<>();
			}
			bodyMatchers.add(matcher);
			return this;
		}

		@Override
		public boolean matches(Object actual) {
			if (!(actual instanceof FunctionCall)) {
				return false;
			}
			FunctionCall vs = (FunctionCall) actual;
			boolean result = functionName.equals(vs.getFunctionName());

			if (bodyMatchers != null) {
				List<QueryExpression> body = vs.getBody();
				if (body.size() == bodyMatchers.size()) {
					for (int i = 0; i < body.size(); i++) {
						result &= bodyMatchers.get(i).matches(body.get(i));
					}
				} else {
					result = false;
				}
			}

			return result;
		}

		@Override
		public void describeTo(Description description) {
			// TODO Auto-generated method stub

		}

	}

	private static class VSMatcher extends BaseMatcher<QueryElement> {

		private String metric;
		private List<LabelMatcher> labels;

		public VSMatcher(String metric) {
			super();
			this.metric = metric;
		}

		public VSMatcher addLabel(String key, String value) {
			return addLabel(key, "=", value);
		}

		public VSMatcher addLabel(String key, String op, String value) {
			if (labels == null) {
				labels = new ArrayList<>();
			}
			labels.add(new LabelMatcher(key, op, value));
			return this;
		}

		@Override
		public boolean matches(Object actual) {
			if (!(actual instanceof VectorSelector)) {
				return false;
			}
			VectorSelector vs = (VectorSelector) actual;
			boolean result = metric.equals(vs.getMetric());
			if (labels != null) {
				List<LabelMatcher> labelMatcher = vs.getLabelMatcher();
				result &= CollectionUtils.containsAll(labelMatcher, this.labels);
			}

			return result;
		}

		@Override
		public void describeTo(Description description) {
			description.appendText(metric);

		}

	}

	@AllArgsConstructor
	private static class SQMatcher extends BaseMatcher<QueryElement> {
		private Matcher<QueryElement> matcher;
		private String from;
		private String to;

		@Override
		public boolean matches(Object actual) {
			if (actual instanceof SubQuery) {
				SubQuery pe = (SubQuery) actual;
				boolean result = matcher.matches(pe.getExpression());
				result &= from.equals(pe.getFrom());
				if (to == null) {
					result &= pe.getTo() == null;
				} else {
					result &= to.equals(pe.getTo());
				}
				return result;
			}
			return false;
		}

		@Override
		public void describeTo(Description description) {

		}

	}

	@AllArgsConstructor
	private static class ParenExpressionMatcher extends BaseMatcher<QueryElement> {
		private Matcher<QueryElement> matcher;

		@Override
		public boolean matches(Object actual) {
			if (actual instanceof ParenExpression) {
				ParenExpression pe = (ParenExpression) actual;
				return matcher.matches(pe.getExpression());
			}
			return false;
		}

		@Override
		public void describeTo(Description description) {

		}

	}

	private static class AEMatcher extends BaseMatcher<QueryElement> {
		private String function;
		private String modifier;
		private List<BaseMatcher<QueryElement>> bodyMatchers;
		private List<String> expectedGroupings;

		public AEMatcher(String function) {
			super();
			this.function = function;
		}

		public AEMatcher(String function, String modifier) {
			super();
			this.function = function;
			this.modifier = modifier;
		}

		public AEMatcher addBodyMatcher(BaseMatcher<QueryElement> matcher) {
			if (bodyMatchers == null) {
				bodyMatchers = new ArrayList<>();
			}
			bodyMatchers.add(matcher);
			return this;
		}

		public AEMatcher addGrouping(String grouping) {
			if (expectedGroupings == null) {
				expectedGroupings = new ArrayList<>();
			}
			expectedGroupings.add(grouping);
			return this;
		}

		@Override
		public boolean matches(Object actual) {
			if (!(actual instanceof AggregateExpression)) {
				return false;
			}
			AggregateExpression vs = (AggregateExpression) actual;
			boolean result = function.equals(vs.getFunctionName());
			if (modifier != null) {
				result &= modifier.equals(vs.getModifier());
			}

			if (bodyMatchers != null) {
				List<QueryExpression> body = vs.getBody();
				if (body.size() == bodyMatchers.size()) {
					for (int i = 0; i < body.size(); i++) {
						result &= bodyMatchers.get(i).matches(body.get(i));
					}
				} else {
					result = false;
				}
			}

			if (expectedGroupings != null) {
				List<String> groupings = vs.getGroupingLabels();
				if (groupings.size() == expectedGroupings.size()) {
					for (int i = 0; i < groupings.size(); i++) {
						result &= expectedGroupings.get(i).matches(groupings.get(i));
					}
				} else {
					result = false;
				}
			}

			return result;
		}

		@Override
		public void describeTo(Description description) {
			// TODO Auto-generated method stub

		}

	}

	@AllArgsConstructor
	private static class UEMatcher extends BaseMatcher<QueryElement> {

		private Matcher<QueryElement> matcher;
		private String op;

		@Override
		public boolean matches(Object actual) {
			if (actual instanceof UnaryExpression) {
				UnaryExpression up = (UnaryExpression) actual;
				boolean result = op.equals(up.getOp());
				result &= matcher.matches(up.getExpr());
				return result;
			}
			return false;
		}

		@Override
		public void describeTo(Description description) {

		}

	}

	@AllArgsConstructor
	private static class BEMatcher extends BaseMatcher<QueryElement> {
		private Matcher<QueryElement> lhMatcher;
		private String op;
		private Matcher<QueryElement> rhMatcher;
		private Boolean returnBool;

		@Override
		public boolean matches(Object actual) {
			if (actual instanceof BinaryExpression) {
				BinaryExpression nl = (BinaryExpression) actual;
				boolean result = lhMatcher.matches(nl.getLhs());
				result &= op.equals(nl.getOperator());
				result &= rhMatcher.matches(nl.getRhs());
				if (returnBool != null)
					result &= returnBool.equals(nl.isReturnBool());
				return result;
			}
			return false;
		}

		@Override
		public void describeTo(Description description) {
			description.appendText("(").appendDescriptionOf(lhMatcher).appendText(" ").appendText(op).appendText(" ")
					.appendDescriptionOf(rhMatcher).appendText(")");
		}

		public BEMatcher(Matcher<QueryElement> lhMatcher, String op, Matcher<QueryElement> rhMatcher) {
			super();
			this.lhMatcher = lhMatcher;
			this.op = op;
			this.rhMatcher = rhMatcher;
		}

	}

	private static class NLMatcher extends BaseMatcher<QueryElement> {
		private Double expectedValue;
		private boolean positiveInfinite;

		public NLMatcher(double d) {
			expectedValue = d;
		}

		public NLMatcher(boolean positiveInfinite) {
			this.positiveInfinite = positiveInfinite;
		}

		@Override
		public boolean matches(Object actual) {
			if (actual instanceof NumberLiteral) {
				NumberLiteral nl = (NumberLiteral) actual;
				if (expectedValue != null)
					return nl.getValue() == expectedValue;
				else
					return positiveInfinite == nl.getIsPositiveInfinty().booleanValue();
			}
			return false;
		}

		@Override
		public void describeTo(Description description) {
			description.appendText("is NumberLiteral with value ").appendValue(expectedValue);
		}

	}

	private static class SLMatcher extends BaseMatcher<QueryElement> {
		private String expectedValue;

		public SLMatcher(String expectedValue) {
			super();
			this.expectedValue = expectedValue;
		}

		@Override
		public boolean matches(Object actual) {
			if (actual instanceof StringLiteral) {
				StringLiteral nl = (StringLiteral) actual;
				return expectedValue.equals(nl.getValue());
			}
			return false;
		}

		@Override
		public void describeTo(Description description) {
			description.appendText("is StringLiteral with value ").appendValue(expectedValue);
		}

	}



}
