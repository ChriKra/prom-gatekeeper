package de.chrikra.query;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import de.chrikra.query.PromQLParser;

public class PromQLParserTest {

	@Test
	public void testMetric() throws IOException {
		QueryElement elem = parse("abc");
		assertThat(elem, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) elem;
		assertThat(m.getMetric(), equalTo("abc"));
		assertThat(m.getLabelMatcher(), nullValue());
	}

	@Test
	public void testMetricWithLabel() throws IOException {
		QueryElement elem = parse("abc{instance=\"mysql\"}");
		assertThat(elem, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) elem;
		assertThat(m.getMetric(), equalTo("abc"));
		assertThat(m.getLabelMatcher(), contains(new LabelMatcher("instance", "=", "mysql")));
	}

	@Test
	public void testMetricWithRegExLabel() throws IOException {
		QueryElement elem = parse("http_requests_total{job=~\".*server\"}");
		assertThat(elem, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) elem;
		assertThat(m.getMetric(), equalTo("http_requests_total"));
		assertThat(m.getLabelMatcher(), contains(new LabelMatcher("job", "=~", ".*server")));
	}

	@Test
	public void testMetricWithIPLabel() throws IOException {
		QueryElement elem = parse("abc{instance=\"127.0.0.1:8080\"}");
		assertThat(elem, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) elem;
		assertThat(m.getMetric(), equalTo("abc"));
		assertThat(m.getLabelMatcher(), contains(new LabelMatcher("instance", "=", "127.0.0.1:8080")));
	}

	@Test
	public void testMetricWithLabelAndComma() throws IOException {
		QueryElement elem = parse("abc{instance=\"mysql\",}");
		assertThat(elem, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) elem;
		assertThat(m.getMetric(), equalTo("abc"));
		assertThat(m.getLabelMatcher(), contains(new LabelMatcher("instance", "=", "mysql")));
	}

	@Test
	public void testMetricWithEmptyLabel() throws IOException {
		QueryElement elem = parse("abc{}");
		assertThat(elem, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) elem;
		assertThat(m.getMetric(), equalTo("abc"));
		assertThat(m.getLabelMatcher(), nullValue());
	}

	@Test
	public void testMetricWithTwoLabels() throws IOException {
		QueryElement elem = parse("abc{instance=\"mysql\",foo=\"bar\"}");
		assertThat(elem, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) elem;
		assertThat(m.getMetric(), equalTo("abc"));
		assertThat(m.getLabelMatcher(),
				contains(new LabelMatcher("instance", "=", "mysql"), new LabelMatcher("foo", "=", "bar")));
	}

	@Test
	public void testMetricWithRangeSelector() throws IOException {
		QueryElement elem = parse("abc[5m]");
		assertThat(elem, instanceOf(MatrixSelector.class));
		MatrixSelector ms = (MatrixSelector) elem;
		assertThat(ms.getDuration(), equalTo("5m"));
		elem = ms.getExpression();
		assertThat(elem, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) elem;
		assertThat(m.getMetric(), equalTo("abc"));
		assertThat(m.getLabelMatcher(), nullValue());
	}

	@Test
	public void testFunction() throws IOException {
		QueryElement elem = parse("rate(http_requests_total)");
		assertThat(elem, instanceOf(FunctionCall.class));
		FunctionCall functionCall = (FunctionCall) elem;
		assertThat(functionCall.getFunctionName(), equalTo("rate"));
		assertThat(functionCall.getBody(), hasSize(1));
		QueryElement queryElement = functionCall.getBody().get(0);
		assertThat(queryElement, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) queryElement;
		assertThat(m.getMetric(), equalTo("http_requests_total"));
		assertThat(m.getLabelMatcher(), nullValue());
	}

	@Test
	public void testFunctionWihoutParam() throws IOException {
		QueryElement elem = parse("rate()");
		assertThat(elem, instanceOf(FunctionCall.class));
		FunctionCall functionCall = (FunctionCall) elem;
		assertThat(functionCall.getFunctionName(), equalTo("rate"));
		assertThat(functionCall.getBody(), nullValue());
	}

	@Test
	public void testFunctionWithMultipleParams() throws IOException {
		QueryElement elem = parse("rate(http_requests_total, test)");
		assertThat(elem, instanceOf(FunctionCall.class));
		FunctionCall functionCall = (FunctionCall) elem;
		assertThat(functionCall.getFunctionName(), equalTo("rate"));
		assertThat(functionCall.getBody(), hasSize(2));
		QueryElement queryElement = functionCall.getBody().get(0);
		assertThat(queryElement, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) queryElement;
		assertThat(m.getMetric(), equalTo("http_requests_total"));
		assertThat(m.getLabelMatcher(), nullValue());
		queryElement = functionCall.getBody().get(1);
		assertThat(queryElement, instanceOf(VectorSelector.class));
		m = (VectorSelector) queryElement;
		assertThat(m.getMetric(), equalTo("test"));
		assertThat(m.getLabelMatcher(), nullValue());
	}

	@Test
	public void testFunctionWithRangeSelector() throws IOException {
		QueryElement elem = parse("rate(http_requests_total[5m])");
		assertThat(elem, instanceOf(FunctionCall.class));
		FunctionCall functionCall = (FunctionCall) elem;
		assertThat(functionCall.getFunctionName(), equalTo("rate"));
		assertThat(functionCall.getBody(), hasSize(1));
		QueryElement queryElement = functionCall.getBody().get(0);
		assertThat(queryElement, instanceOf(MatrixSelector.class));
		MatrixSelector m = (MatrixSelector) queryElement;
		assertThat(m.getExpression(), instanceOf(VectorSelector.class));
		VectorSelector vs = (VectorSelector) m.getExpression();
		assertThat(vs.getMetric(), equalTo("http_requests_total"));
		assertThat(vs.getLabelMatcher(), nullValue());
		assertThat(m.getDuration(), equalTo("5m"));
	}

	@Test
	public void testFunctionWithRangeSelectorAndLabel() throws IOException {
		QueryElement elem = parse("rate(http_requests_total{instance=\"mysql\",foo=\"bar\"}[5m])");
		assertThat(elem, instanceOf(FunctionCall.class));
		FunctionCall functionCall = (FunctionCall) elem;
		assertThat(functionCall.getFunctionName(), equalTo("rate"));
		assertThat(functionCall.getBody(), hasSize(1));
		QueryElement queryElement = functionCall.getBody().get(0);
		assertThat(queryElement, instanceOf(MatrixSelector.class));
		MatrixSelector m = (MatrixSelector) queryElement;
		assertThat(m.getDuration(), equalTo("5m"));
		assertThat(m.getExpression(), instanceOf(VectorSelector.class));
		VectorSelector vs = (VectorSelector) m.getExpression();
		assertThat(vs.getMetric(), equalTo("http_requests_total"));
		assertThat(vs.getLabelMatcher(),
				contains(new LabelMatcher("instance", "=", "mysql"), new LabelMatcher("foo", "=", "bar")));
	}

	@Test
	public void testAggregateFunction() throws IOException {
		QueryElement elem = parse("avg (http_requests_total)");
		assertThat(elem, instanceOf(AggregateExpression.class));
		AggregateExpression functionCall = (AggregateExpression) elem;
		assertThat(functionCall.getFunctionName(), equalTo("avg"));
		assertThat(functionCall.getBody(), hasSize(1));
		QueryElement queryElement = functionCall.getBody().get(0);
		assertThat(queryElement, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) queryElement;
		assertThat(m.getMetric(), equalTo("http_requests_total"));
	}

	@Test
	public void testAggregateFunctionWithOffset() throws IOException {
		QueryElement elem = parse("sum(http_requests_total{method=\"GET\"} offset 5m)");
		assertThat(elem, instanceOf(AggregateExpression.class));
		AggregateExpression functionCall = (AggregateExpression) elem;
		assertThat(functionCall.getFunctionName(), equalTo("sum"));
		assertThat(functionCall.getBody(), hasSize(1));
		QueryElement queryElement = functionCall.getBody().get(0);
		assertThat(queryElement, instanceOf(OffsetExpression.class));
		OffsetExpression oe = (OffsetExpression) queryElement;
		assertThat(oe.getOffset(), equalTo("5m"));
		assertThat(oe.getExpression(), instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) oe.getExpression();
		assertThat(m.getMetric(), equalTo("http_requests_total"));
		assertThat(m.getLabelMatcher(), contains(new LabelMatcher("method", "=", "GET")));
	}

	@Test
	public void testAggregateFunctionWithModifier() throws IOException {
		QueryElement elem = parse("sum without (instance) (http_requests_total)");
		assertThat(elem, instanceOf(AggregateExpression.class));
		AggregateExpression functionCall = (AggregateExpression) elem;
		assertThat(functionCall.getFunctionName(), equalTo("sum"));
		assertThat(functionCall.getBody(), hasSize(1));
		QueryElement queryElement = functionCall.getBody().get(0);
		assertThat(queryElement, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) queryElement;
		assertThat(m.getMetric(), equalTo("http_requests_total"));
		assertThat(functionCall.getModifier(), equalTo("without"));
		assertThat(functionCall.getGroupingLabels(), contains("instance"));

	}

	@Test
	public void testAggregateFunctionWithMultipleModifier() throws IOException {
		QueryElement elem = parse("sum without (instance, application) (http_requests_total)");
		assertThat(elem, instanceOf(AggregateExpression.class));
		AggregateExpression functionCall = (AggregateExpression) elem;
		assertThat(functionCall.getFunctionName(), equalTo("sum"));
		assertThat(functionCall.getBody(), hasSize(1));
		QueryElement queryElement = functionCall.getBody().get(0);
		assertThat(queryElement, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) queryElement;
		assertThat(m.getMetric(), equalTo("http_requests_total"));
		assertThat(functionCall.getModifier(), equalTo("without"));
		assertThat(functionCall.getGroupingLabels(), contains("instance", "application"));

	}

	@Test
	public void testAggregateFunctionWithModifierWithComma() throws IOException {
		QueryElement elem = parse("sum without (instance,) (http_requests_total)");
		assertThat(elem, instanceOf(AggregateExpression.class));
		AggregateExpression functionCall = (AggregateExpression) elem;
		assertThat(functionCall.getFunctionName(), equalTo("sum"));
		assertThat(functionCall.getBody(), hasSize(1));
		QueryElement queryElement = functionCall.getBody().get(0);
		assertThat(queryElement, instanceOf(VectorSelector.class));
		VectorSelector m = (VectorSelector) queryElement;
		assertThat(m.getMetric(), equalTo("http_requests_total"));
		assertThat(functionCall.getModifier(), equalTo("without"));
		assertThat(functionCall.getGroupingLabels(), contains("instance"));
	}



	private QueryElement parse(String string) throws IOException {
		return new PromQLParser().parse(string);
	}
}
