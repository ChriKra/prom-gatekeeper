lexer grammar PromQLLexer;

fragment DIGIT : [0-9];
fragment LETTER : [A-Za-z];
fragment DOT : '.';
fragment QUOTE: '"';
fragment SINGLE_QUOTE: '\'';
fragment INT : (MINUS)? DIGIT+;
fragment ALPHANUMERIC: LETTER | '_' | DIGIT;
fragment STRINGCHAR: LETTER | DIGIT | ':' | DOT | COMMA;
fragment A : [aA]; // match either an 'a' or 'A'
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];

/*
	Keywords
*/
// Operators
LAND : A N D;
LOR : O R;
LUNLESS : U N L E S S;

//Aggregators
SUM : S U M;
AVG : A V G;
COUNT: C O U N T;
MIN : M I N;
MAX : M A X;
STDDEV : S T D D E V;
STDVAR : S T D V A R;
TOPK : T O P K;
BOTTOMK: B O T T O M K;
COUNT_VALUES : C O U N T '_' V A L U E S;
QUANTILE : Q U A N T I L E;

//Keywords
OFFSET: O F F S E T -> pushMode(OFFSET_MODE);
BY: B Y;
WITHOUT: W I T H O U T;
ON : O N;
IGNORING: I G N O R I N G;
GROUP_LEFT : G R O U P '_' L E F T;
GROUP_RIGHT : G R O U P '_' R I G H T;
BOOL : B O O L;


STRING_LITERAL: '"' (~["] | '\\"')* '"';
CHAR_LITERAL: '\'' (~['] | '\\\'')* '\'';
BACKTICK_LITERAL: '`' (~[`] | '\\`')* '`';
EXP_NUMBER: NUMBER 'e' INT;
OCT_NUMBER: '0' DIGIT+;
HEX_NUMBER: '0x' (DIGIT | 'a' | 'b' | 'c' | 'd' | 'e' | 'f')+;
NUMBER: (DIGIT+ DOT? DIGIT*) | (DOT DIGIT+);
INF : 'Inf';
MINUS : '-';
PLUS : '+';
DIV : '/';
MUL : '*';
DOUBLE_EQL : '==';
EQL : '=';
NEQ : '!=';
GTR : '>=';
GTE : '>';
LTE : '<=';
LSS : '<';
MOD : '%';
POW : '^';
COMMA : ',';
LEFT_PAREN : '(';
LEFT_BRACE : '{';
LEFT_BRACKET : '[' -> pushMode(DURATION_MODE);
RIGHT_PAREN : ')';
RIGHT_BRACE : '}';
EQL_REGEX: '=~';
NEQ_REGEX: '!~';
METRIC_IDENTIFIER: (':' | ALPHANUMERIC)+;
WS: [ \n\t\r] -> skip;

mode DURATION_MODE;
COLON : ':';
DURATION: DIGIT+ ('m' | 'h' | 's' | 'd' | 'y' | 'ms' | 'w');
RIGHT_BRACKET : ']' -> popMode;
WS2: [ \n\t\r] -> skip;

mode OFFSET_MODE;
ODURATION: DIGIT+ ('m' | 'h' | 's' | 'd' | 'y' | 'ms' | 'w') -> popMode;
WS3: [ \n\t\r] -> skip;
