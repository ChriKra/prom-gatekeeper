parser grammar PromQLParser;

options { tokenVocab=PromQLLexer; }


start: expr EOF;
	  
expr: 	aggregate_expr #aggregate_expr2
		| function_call #function_call2
		| number_expr #number_expr2
		| unary_expr #unary_expr2
		| paren_expr #paren_expr2
		| vector_selector #vector_selector2
		| string_literal #string_literal2
		| expr LEFT_BRACKET DURATION RIGHT_BRACKET #matrix_selector
		| expr LEFT_BRACKET DURATION COLON DURATION? RIGHT_BRACKET #subquery_expr
		| expr binary_operator1 group_modifiers? expr #binary_expr
		| expr binary_operator2 group_modifiers? expr #binary_expr
		| expr binary_operator3 group_modifiers? expr #binary_expr
		| expr binary_operator4 group_modifiers? expr #binary_expr
		| expr binary_operator5 group_modifiers? expr #binary_expr
		| expr binary_operator6 group_modifiers? expr #binary_expr
		| expr OFFSET ODURATION #offset_expr
		;

string_literal: STRING_LITERAL | CHAR_LITERAL | BACKTICK_LITERAL;

function_call: METRIC_IDENTIFIER function_call_body;
	  
aggregate_expr: aggregate_op 
				((aggregate_modifier function_call_body)
				|(function_call_body aggregate_modifier?));
aggregate_op: AVG | BOTTOMK | COUNT | COUNT_VALUES | MAX | MIN | QUANTILE | STDDEV | STDVAR | SUM | TOPK ;

aggregate_modifier: (BY | WITHOUT) grouping_labels;
			
grouping_labels: LEFT_PAREN (grouping_label_list COMMA?)? RIGHT_PAREN;

grouping_label_list: grouping_label (COMMA grouping_label)*;

grouping_label: aggregate_op | keywords | operators | METRIC_IDENTIFIER;

keywords: OFFSET | BY | WITHOUT | ON | IGNORING | GROUP_LEFT | GROUP_RIGHT | BOOL;

operators: LAND | LOR | LUNLESS;

function_call_body: LEFT_PAREN function_call_args? RIGHT_PAREN;

function_call_args : expr (COMMA expr)*;

vector_selector: metric_identifier label_matchers?;

metric_identifier: METRIC_IDENTIFIER | SUM | MIN;

label_matchers: LEFT_BRACE (label_match_list COMMA?)? RIGHT_BRACE;

label_match_list: label_matcher (COMMA label_matcher)*;

label_matcher: METRIC_IDENTIFIER match_op string;

match_op: EQL | NEQ | EQL_REGEX | NEQ_REGEX;

paren_expr: LEFT_PAREN expr RIGHT_PAREN;

binary_operator1: (POW );

binary_operator2: (DIV | MUL |  MOD);

binary_operator3: (PLUS | MINUS);

binary_operator4: (DOUBLE_EQL | NEQ | GTR | GTE | LTE | LSS);

binary_operator5: (LAND | LUNLESS);

binary_operator6: LOR;

group_modifiers: bool_modifier;

bool_modifier: BOOL;

number_expr: (PLUS | MINUS) ? (NUMBER | OCT_NUMBER  | HEX_NUMBER | EXP_NUMBER | INF) ;

unary_expr: (PLUS | MINUS) expr;

string: STRING_LITERAL | CHAR_LITERAL;