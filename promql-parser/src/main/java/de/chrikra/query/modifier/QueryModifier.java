package de.chrikra.query.modifier;

import de.chrikra.query.AggregateExpression;
import de.chrikra.query.BinaryExpression;
import de.chrikra.query.FunctionCall;
import de.chrikra.query.MatrixSelector;
import de.chrikra.query.NumberLiteral;
import de.chrikra.query.OffsetExpression;
import de.chrikra.query.ParenExpression;
import de.chrikra.query.StringLiteral;
import de.chrikra.query.SubQuery;
import de.chrikra.query.UnaryExpression;
import de.chrikra.query.VectorSelector;

public interface QueryModifier {

	void modify(FunctionCall functionCall) throws QueryModificationException;

	void modify(AggregateExpression aggregateExpression) throws QueryModificationException;

	void modify(VectorSelector vectorSelector) throws QueryModificationException;

	void modify(UnaryExpression unaryExpression) throws QueryModificationException;

	void modify(SubQuery subQuery) throws QueryModificationException;

	void modify(StringLiteral stringLiteral) throws QueryModificationException;

	void modify(ParenExpression parenExpression) throws QueryModificationException;

	void modify(NumberLiteral numberLiteral) throws QueryModificationException;

	void modify(MatrixSelector matrixSelector) throws QueryModificationException;

	void modify(BinaryExpression binaryExpression);

	void modify(OffsetExpression offsetExpression);

}
