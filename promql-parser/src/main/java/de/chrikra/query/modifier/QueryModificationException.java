package de.chrikra.query.modifier;


public class QueryModificationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5115448271738968712L;

	public QueryModificationException(String message) {
		super(message);
	}
	

	public QueryModificationException() {
		super();
	}


	public QueryModificationException(String message, Throwable cause) {
		super(message, cause);
	}

}
