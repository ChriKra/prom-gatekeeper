package de.chrikra.query.antlr;

import java.util.function.Consumer;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.chrikra.query.AggregateExpression;
import de.chrikra.query.BinaryExpression;
import de.chrikra.query.ConsumerStack;
import de.chrikra.query.FunctionCall;
import de.chrikra.query.LabelMatcher;
import de.chrikra.query.MatrixSelector;
import de.chrikra.query.NumberLiteral;
import de.chrikra.query.OffsetExpression;
import de.chrikra.query.ParenExpression;
import de.chrikra.query.QueryExpression;
import de.chrikra.query.StringLiteral;
import de.chrikra.query.SubQuery;
import de.chrikra.query.UnaryExpression;
import de.chrikra.query.VectorSelector;
import de.chrikra.query.antlr.PromQLParser.Aggregate_exprContext;
import de.chrikra.query.antlr.PromQLParser.Aggregate_modifierContext;
import de.chrikra.query.antlr.PromQLParser.Binary_exprContext;
import de.chrikra.query.antlr.PromQLParser.Binary_operator1Context;
import de.chrikra.query.antlr.PromQLParser.Binary_operator2Context;
import de.chrikra.query.antlr.PromQLParser.Binary_operator3Context;
import de.chrikra.query.antlr.PromQLParser.Binary_operator4Context;
import de.chrikra.query.antlr.PromQLParser.Binary_operator5Context;
import de.chrikra.query.antlr.PromQLParser.Function_callContext;
import de.chrikra.query.antlr.PromQLParser.Group_modifiersContext;
import de.chrikra.query.antlr.PromQLParser.Grouping_labelContext;
import de.chrikra.query.antlr.PromQLParser.Label_matcherContext;
import de.chrikra.query.antlr.PromQLParser.Matrix_selectorContext;
import de.chrikra.query.antlr.PromQLParser.Number_exprContext;
import de.chrikra.query.antlr.PromQLParser.Offset_exprContext;
import de.chrikra.query.antlr.PromQLParser.Paren_exprContext;
import de.chrikra.query.antlr.PromQLParser.String_literalContext;
import de.chrikra.query.antlr.PromQLParser.Subquery_exprContext;
import de.chrikra.query.antlr.PromQLParser.Unary_exprContext;
import de.chrikra.query.antlr.PromQLParser.Vector_selectorContext;

public class PromQLListener extends PromQLParserBaseListener {
	private static final Logger LOG = LoggerFactory.getLogger(PromQLListener.class);
	private ConsumerStack<QueryExpression> exprStack;
	private Consumer<LabelMatcher> labelMatcherConsumer;
	private ConsumerStack<String> groupingLabelStack;
	private QueryExpression result;

	public PromQLListener() {
		exprStack = new ConsumerStack<QueryExpression>(this::setResult);
		groupingLabelStack = new ConsumerStack<String>();
	}

	public void setResult(QueryExpression result) {
		this.result = result;
	}

	public QueryExpression getResult() {
		if (this.result != null) {
			return this.result;
		}

		return null;
	}

	@Override
	public void enterNumber_expr(Number_exprContext ctx) {
		LOG.debug("calling enterNumber_expr");
		Double value = parseDouble(ctx);
		double factor = 1d;
		TerminalNode minus = ctx.MINUS();
		if (minus != null) {
			factor = -1d;
		}

		if (value != null) {
			exprStack.accept(new NumberLiteral(factor * value));
			return;
		}

		TerminalNode token = ctx.getToken(PromQLLexer.INF, 0);
		if (token != null) {
			exprStack.accept(new NumberLiteral(minus == null));
			return;
		}
	}

	private Double parseDouble(Number_exprContext ctx) {
		TerminalNode token = ctx.getToken(PromQLLexer.NUMBER, 0);
		if (token != null) {
			return Double.parseDouble(token.getText());
		}

		token = ctx.getToken(PromQLLexer.EXP_NUMBER, 0);
		if (token != null) {
			return Double.parseDouble(token.getText());
		}

		token = ctx.getToken(PromQLLexer.OCT_NUMBER, 0);
		if (token != null) {
			String valueText = token.getText().substring(1);
			int res = Integer.parseInt(valueText, 8);
			return (double) res;
		}

		token = ctx.getToken(PromQLLexer.HEX_NUMBER, 0);
		if (token != null) {
			String valueText = token.getText().substring(2);
			int res = Integer.parseInt(valueText, 16);
			return (double) res;
		}
		return null;
	}

	@Override
	public void enterBinary_expr(Binary_exprContext ctx) {
		LOG.debug("calling enterBinary_expr");

		ParserRuleContext operatorCtx = getOperator(ctx);
		String operator = operatorCtx.getText();

		BinaryExpression be = new BinaryExpression();

		be.setOperator(operator);
		Group_modifiersContext group_modifiers = ctx.group_modifiers();
		if (group_modifiers != null) {
			String text = group_modifiers.getText();
			be.setReturnBool("bool".equals(text));
		}
		this.exprStack.accept(be);
		this.exprStack.addConsumer(be::addExpr);
	}

	private ParserRuleContext getOperator(Binary_exprContext ctx) {
		Binary_operator1Context binary_operator1 = ctx.binary_operator1();
		if (binary_operator1 != null) {
			return binary_operator1;
		}
		Binary_operator2Context binary_operator2 = ctx.binary_operator2();
		if (binary_operator2 != null) {
			return binary_operator2;
		}
		Binary_operator3Context binary_operator3 = ctx.binary_operator3();
		if (binary_operator3 != null) {
			return binary_operator3;
		}
		Binary_operator4Context binary_operator4 = ctx.binary_operator4();
		if (binary_operator4 != null) {
			return binary_operator4;
		}
		Binary_operator5Context binary_operator5 = ctx.binary_operator5();
		if (binary_operator5 != null) {
			return binary_operator5;
		}

		return ctx.binary_operator6();
	}

	@Override
	public void exitBinary_expr(Binary_exprContext ctx) {
		LOG.debug("calling exitBinary_expr");
		this.exprStack.popConsumer();
	}

	@Override
	public void enterUnary_expr(Unary_exprContext ctx) {
		LOG.debug("calling enterUnary_expr");
		String op = "-";
		if (ctx.PLUS() != null) {
			op = "+";
		}
		UnaryExpression expr = new UnaryExpression();
		expr.setOp(op);
		this.exprStack.accept(expr);
		this.exprStack.addConsumer(expr::setExpr);
	}

	@Override
	public void exitUnary_expr(Unary_exprContext ctx) {
		LOG.debug("calling enterUnary_expr");
		this.exprStack.popConsumer();
	}

	@Override
	public void enterParen_expr(Paren_exprContext ctx) {
		LOG.debug("calling enterParen_expr");
		ParenExpression pe = new ParenExpression();
		this.exprStack.accept(pe);
		this.exprStack.addConsumer(pe::setExpression);
	}

	@Override
	public void exitParen_expr(Paren_exprContext ctx) {
		LOG.debug("calling exitParen_expr");
		this.exprStack.popConsumer();
	}

	@Override
	public void enterVector_selector(Vector_selectorContext ctx) {
		LOG.debug("calling enterVector_selector");
		VectorSelector vs = new VectorSelector();
		vs.setMetric(ctx.metric_identifier().getText());
		this.labelMatcherConsumer = vs::addLabelMatcher;
		this.exprStack.accept(vs);
	}

	@Override
	public void exitVector_selector(Vector_selectorContext ctx) {
		LOG.debug("calling exitVector_selector");
		this.labelMatcherConsumer = null;
	}

	@Override
	public void enterAggregate_expr(Aggregate_exprContext ctx) {
		LOG.debug("calling enterAggregate_expr");
		AggregateExpression ae = new AggregateExpression();
		String op = ctx.aggregate_op().getText();
		ae.setFunctionName(op);
		Aggregate_modifierContext aggregate_modifier = ctx.aggregate_modifier();
		if (aggregate_modifier != null) {
			String modifier = null;
			TerminalNode by = aggregate_modifier.BY();
			if (by != null) {
				modifier = by.getText();
			}

			TerminalNode without = aggregate_modifier.WITHOUT();
			if (without != null) {
				modifier = without.getText();
			}
			ae.setModifier(modifier);
		}
		this.exprStack.accept(ae);
		this.exprStack.addConsumer(ae::addBodyExpression);
		this.groupingLabelStack.addConsumer(ae::addGroupingLabel);
	}

	@Override
	public void exitAggregate_expr(Aggregate_exprContext ctx) {
		LOG.debug("calling exitAggregate_expr");
		this.exprStack.popConsumer();
		this.groupingLabelStack.popConsumer();
	}

	@Override
	public void enterFunction_call(Function_callContext ctx) {
		LOG.debug("calling enterFunction_call");
		FunctionCall fc = new FunctionCall();
		String text = ctx.METRIC_IDENTIFIER().getText();
		fc.setFunctionName(text);
		this.exprStack.accept(fc);
		this.exprStack.addConsumer(fc::addBodyExpression);
	}

	@Override
	public void exitFunction_call(Function_callContext ctx) {
		LOG.debug("calling exitFunction_call");
		this.exprStack.popConsumer();
	}

	@Override
	public void enterString_literal(String_literalContext ctx) {
		LOG.debug("calling enterString_literal");
		String text = ctx.getText();
		StringLiteral sl = new StringLiteral(text.substring(1, text.length() - 1));
		this.exprStack.accept(sl);
	}

	@Override
	public void enterMatrix_selector(Matrix_selectorContext ctx) {
		LOG.debug("calling enterMatrix_selector");
		TerminalNode duration = ctx.DURATION();
		MatrixSelector ms = new MatrixSelector();
		ms.setDuration(duration.getText());
		this.exprStack.accept(ms);
		this.exprStack.addConsumer(ms::setExpression);
	}

	@Override
	public void exitMatrix_selector(Matrix_selectorContext ctx) {
		LOG.debug("calling exitMatrix_selector");
		this.exprStack.popConsumer();
	}

	@Override
	public void enterSubquery_expr(Subquery_exprContext ctx) {
		LOG.debug("calling enterSubquery_expr");
		TerminalNode from = ctx.DURATION(0);
		TerminalNode to = ctx.DURATION(1);
		SubQuery sq = new SubQuery();
		sq.setFrom(from.getText());
		sq.setTo(to != null ? to.getText() : null);
		this.exprStack.accept(sq);
		this.exprStack.addConsumer(sq::setExpression);
	}

	@Override
	public void exitSubquery_expr(Subquery_exprContext ctx) {
		LOG.debug("calling exitSubquery_expr");
		this.exprStack.popConsumer();
	}

	@Override
	public void enterOffset_expr(Offset_exprContext ctx) {
		LOG.debug("calling enterOffset_expr");
		String duration = ctx.ODURATION().getText();
		OffsetExpression oe = new OffsetExpression();
		oe.setOffset(duration);
		this.exprStack.accept(oe);
		this.exprStack.addConsumer(oe::setExpression);
	}

	@Override
	public void exitOffset_expr(Offset_exprContext ctx) {
		LOG.debug("calling exitOffset_expr");
		this.exprStack.popConsumer();
	}

	@Override
	public void enterGrouping_label(Grouping_labelContext ctx) {
		LOG.debug("calling enterGrouping_label");
		String text = ctx.getText();
		this.groupingLabelStack.accept(text);
	}

	@Override
	public void enterLabel_matcher(Label_matcherContext ctx) {
		LOG.debug("calling enterLabel_matcher");
		String metric = ctx.METRIC_IDENTIFIER().getText();
		String op = ctx.match_op().getText();
		String value = ctx.string().getText();
		LabelMatcher lm = new LabelMatcher(metric, op, value.substring(1, value.length() - 1));
		this.labelMatcherConsumer.accept(lm);
	}
}
