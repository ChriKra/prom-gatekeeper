package de.chrikra.query;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import lombok.Data;



@Data
public class UnaryExpression implements QueryExpression {
	private String op;
	private QueryExpression expr;

	@Override
	public void apply(QueryModifier modifier) throws QueryModificationException {
		modifier.modify(this);
	}

	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder(op);
		sb.append(" ");
		sb.append(expr.getQueryString());
		return sb.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(op);
		sb.append(" ");
		sb.append(expr.toString());
		return sb.toString();
	}
}
