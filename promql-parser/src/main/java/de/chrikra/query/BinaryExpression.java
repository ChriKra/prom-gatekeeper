package de.chrikra.query;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import lombok.Data;

@Data
public class BinaryExpression implements QueryExpression {
	private QueryExpression lhs;
	private String operator;
	private QueryExpression rhs;
	private boolean returnBool;

	@Override
	public void apply(QueryModifier modifier) throws QueryModificationException {
		modifier.modify(this);
	}

	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder();
		sb.append(lhs.getQueryString());
		sb.append(" ");
		sb.append(operator);
		sb.append(" ");
		if(returnBool) {
			sb.append("bool ");
		}
		sb.append(rhs.getQueryString());
		return sb.toString();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("(");
		sb.append(lhs.toString());
		sb.append(" ");
		sb.append(operator);
		sb.append(" ");
		if(returnBool) {
			sb.append("bool ");
		}
		sb.append(rhs.toString());
		sb.append(")");
		return sb.toString();
	}
	
	public void addExpr(QueryExpression qe) {
		if(lhs == null) {
			lhs = qe;
		}
		else if(rhs == null) {
			rhs = qe;
		}
		else {
			throw new IllegalStateException("Both expression have been set before!");
		}
	}
}
