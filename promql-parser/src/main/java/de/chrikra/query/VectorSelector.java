package de.chrikra.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import lombok.Data;

@Data
public class VectorSelector implements QueryExpression{
	private String metric;
	private List<LabelMatcher> labelMatcher;
	
	@Override
	public void apply(QueryModifier modifier) throws QueryModificationException {
		modifier.modify(this);
	} 
	
	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder();
		sb.append(metric);
		if(CollectionUtils.isNotEmpty(this.labelMatcher)) {
			sb.append("{");
			sb.append(StringUtils.join(this.labelMatcher, ','));
			sb.append("}");
		}
		return sb.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(metric);
		if(CollectionUtils.isNotEmpty(this.labelMatcher)) {
			sb.append("{");
			sb.append(StringUtils.join(this.labelMatcher, ','));
			sb.append("}");
		}
		return sb.toString();
	}
	
	public void addLabelMatcher(LabelMatcher lb) {
		if(this.labelMatcher == null) {
			this.labelMatcher = new ArrayList<>();
		}
		this.labelMatcher.add(lb);
	}
}
