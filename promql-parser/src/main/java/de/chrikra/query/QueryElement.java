package de.chrikra.query;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;

public interface QueryElement {
	public void apply(QueryModifier modifier) throws QueryModificationException;
	
	public String getQueryString();
}
