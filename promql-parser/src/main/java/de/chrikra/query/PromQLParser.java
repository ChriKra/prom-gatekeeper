package de.chrikra.query;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import de.chrikra.query.antlr.PromQLLexer;
import de.chrikra.query.antlr.PromQLListener;
import de.chrikra.query.antlr.PromQLParser.StartContext;

public class PromQLParser {
	public QueryElement parse(String query) {
		PromQLListener listener = new PromQLListener();
		PromQLLexer lexer = new PromQLLexer(CharStreams.fromString(query));

		ThrowExceptionErrorListener errorListener = new ThrowExceptionErrorListener();
		lexer.removeErrorListeners();
		lexer.addErrorListener(errorListener);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		de.chrikra.query.antlr.PromQLParser parser = new de.chrikra.query.antlr.PromQLParser(tokens);
		parser.removeErrorListeners();

		parser.addErrorListener(errorListener);
		StartContext start = parser.start();
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(listener, start);
		return listener.getResult();
	}
}
