package de.chrikra.query;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class MatrixSelector implements QueryExpression {
	private QueryExpression expression;
	private String duration;

	public MatrixSelector() {
	}
	
	@Override
	public void apply(QueryModifier modifier) throws QueryModificationException {
		modifier.modify(this);
	}

	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder(expression.getQueryString());
		sb.append("[");
		sb.append(duration);
		sb.append("]");
		return sb.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(expression.toString());
		sb.append("[");
		sb.append(duration);
		sb.append("]");
		return sb.toString();
	}
}
