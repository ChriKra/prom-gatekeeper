package de.chrikra.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AggregateExpression implements QueryExpression {
	private String functionName;
	private List<QueryExpression> body;
	private String modifier;
	private List<String> groupingLabels;

	@Override
	public void apply(QueryModifier modifier) throws QueryModificationException {
		modifier.modify(this);
	}

	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder();
		sb.append(functionName);
		sb.append("(");
		if (CollectionUtils.isNotEmpty(body)) {
			sb.append(StringUtils.join(body.stream().map(QueryExpression::getQueryString), ','));
		}
		sb.append(")");

		if (modifier != null) {
			sb.append(" ");
			sb.append(modifier);
			sb.append(" ");
		}

		if (CollectionUtils.isNotEmpty(groupingLabels)) {
			sb.append(StringUtils.join(groupingLabels, ','));
		}
		return sb.toString();
	}

	public void addGroupingLabel(String label) {
		if (this.groupingLabels == null) {
			this.groupingLabels = new ArrayList<>();
		}
		this.groupingLabels.add(label);
	}

	public void addBodyExpression(QueryExpression t) {
		if (this.body == null) {
			this.body = new ArrayList<>();
		}
		this.body.add(t);
	}

	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(functionName);
		sb.append("(");
		if (CollectionUtils.isNotEmpty(body)) {
			sb.append(StringUtils.join(body, ','));
		}
		sb.append(")");

		if (modifier != null) {
			sb.append(" ");
			sb.append(modifier);
			sb.append(" ");
		}

		if (CollectionUtils.isNotEmpty(groupingLabels)) {
			sb.append(StringUtils.join(groupingLabels, ','));
		}
		return sb.toString();
	}
}
