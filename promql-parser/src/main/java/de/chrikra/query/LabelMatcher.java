package de.chrikra.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LabelMatcher {
	private String identifier;
	private String op;
	private String value;
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(identifier);
		sb.append(op);
		sb.append("\"");
		sb.append(value);
		sb.append("\"");
		return sb.toString();
	}
}
