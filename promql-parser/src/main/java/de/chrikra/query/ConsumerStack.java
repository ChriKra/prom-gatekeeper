package de.chrikra.query;

import java.util.Stack;
import java.util.function.Consumer;

public class ConsumerStack<T> {
	private Consumer<T> current;
	private Stack<Consumer<T>> stack;
	private T lastElement;

	public ConsumerStack() {
		this.stack = new Stack<>();
	}

	public ConsumerStack(Consumer<T> current) {
		super();
		this.current = current;
		this.stack = new Stack<>();
		this.stack.push(current);
	}

	public void accept(T t) {
		current.accept(t);
		this.lastElement = t;
	}

	public void addConsumer(Consumer<T> consumer) {
		this.stack.push(consumer);
		this.current = consumer;
	}

	public Consumer<T> popConsumer() {
		Consumer<T> pop = stack.pop();
		if (stack.isEmpty()) {
			current = null;
		} else {
			current = stack.peek();
		}
		return pop;
	}

	public T getLastElement() {
		return lastElement;
	}

}
