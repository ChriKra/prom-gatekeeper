package de.chrikra.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import lombok.Data;

@Data
public class FunctionCall implements QueryExpression {
	private String functionName;
	private List<QueryExpression> body;

	@Override
	public void apply(QueryModifier modifier) throws QueryModificationException {
		modifier.modify(this);
	}

	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder(functionName);
		sb.append("(");
		if (body != null) {
			sb.append(StringUtils.join(body.stream().map(QueryElement::getQueryString), ','));
		}
		sb.append(")");
		return sb.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(functionName);
		sb.append("(");
		if (body != null) {
			sb.append(StringUtils.join(body, ','));
		}
		sb.append(")");
		return sb.toString();
	}


	public void addBodyExpression(QueryExpression t) {
		if(this.body == null) {
			this.body = new ArrayList<>();
		}
		this.body.add(t);
	}


}
