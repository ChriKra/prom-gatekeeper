package de.chrikra.query;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import lombok.Data;

@Data
public class ParenExpression implements QueryExpression{
	private QueryExpression expression;
	
	
	@Override
	public void apply(QueryModifier modifier) throws QueryModificationException {
		modifier.modify(this);
		
	}
	
	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder("(");
		sb.append(expression.getQueryString());
		sb.append(")");
		return sb.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("(");
		sb.append(expression.toString());
		sb.append(")");
		return sb.toString();
	}

}
