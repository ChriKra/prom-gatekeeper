package de.chrikra.query;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import lombok.Data;

@Data
public class OffsetExpression implements QueryExpression{
	private QueryExpression expression;
	private String offset;
	
	@Override
	public void apply(QueryModifier modifier) throws QueryModificationException {
		modifier.modify(this);
	}

	@Override
	public String getQueryString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.expression.getQueryString());
		sb.append(" offset ");
		sb.append(offset);
		return sb.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.expression.toString());
		sb.append(" offset ");
		sb.append(offset);
		return sb.toString();
	}

}
