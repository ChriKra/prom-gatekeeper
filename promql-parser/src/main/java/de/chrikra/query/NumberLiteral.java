package de.chrikra.query;

import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import lombok.Getter;


@Getter
public class NumberLiteral implements QueryExpression{
	private double value;
	private Boolean isPositiveInfinty;

	public NumberLiteral(double value) {
		super();
		this.value = value;
	}

	public NumberLiteral(Boolean isPositiveInfinty) {
		super();
		this.isPositiveInfinty = isPositiveInfinty;
	}
	
	@Override
	public void apply(QueryModifier modifier) throws QueryModificationException {
		modifier.modify(this);
	}

	@Override
	public String getQueryString() {
		return Double.toString(value);
	}
	
	@Override
	public String toString() {
		return Double.toString(value);
	}

}
