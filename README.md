# prom-gatekeeper

## Description
This service acts as middleware between [Prometheus](https://prometheus.io/) and calling clients and provides an access control layer to restrict the users access to the API of prometheus. This makes it possible, for example, to give a user only access to the metrics of individual instances or jobs.

Prom-gatekeeper offers the same API as Prometheus, so it is possible to use existing clients and tools for Prometheus such as the wonderful [Grafana](https://grafana.com/). The only requirement towards the client is the support of http basic authentication (without authentication only the "anon" user can be used).

## Configuration
TBD

## Query rewriting
TBD 
