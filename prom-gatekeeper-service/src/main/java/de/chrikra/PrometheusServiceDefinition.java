package de.chrikra;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface PrometheusServiceDefinition {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("query")
	public Response query(@QueryParam("query") String query, @QueryParam("time") String time,
			@QueryParam("timeout") String timeout);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("query")
	public Response queryPost(@FormParam("query") String query, @FormParam("time") String time,
			@FormParam("timeout") String timeout);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("query_range")
	public Response queryRange(@QueryParam("query") String query, @QueryParam("start") String start,
			@QueryParam("end") String end, @QueryParam("step") String step, @QueryParam("timeout") String timeout);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("query_range")
	public Response queryRangePost(@FormParam("query") String query, @FormParam("start") String start,
			@FormParam("end") String end, @FormParam("step") String step, @FormParam("timeout") String timeout);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("series")
	public Response series(@QueryParam("match[]") String match, @QueryParam("start") String start,
			@QueryParam("end") String end);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("series")
	public Response seriesPost(@FormParam("match") String match, @FormParam("start") String start,
			@FormParam("end") String end);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("labels")
	public Response labels();

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("labels")
	public Response labelsPost();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("label/{label_name}/values")
	public Response labelValues(@PathParam("label_name") String labelName);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("targets")
	public Response targets(@QueryParam("state") String state);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("rules")
	public Response rules(@QueryParam("type") String type);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("alerts")
	public Response alerts();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("targets/metadata")
	public Response targetMetaData(@QueryParam("match_target") String matchTarget, @QueryParam("metric") String metric,
			@QueryParam("limit") Integer limit);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("metadata")
	public Response metaData(@QueryParam("limit") Integer limit, @QueryParam("metric") String metric);
}
