package de.chrikra;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.HttpStatus;

import de.chrikra.dto.LabelValueResult;
import de.chrikra.dto.MetaDataEntry;
import de.chrikra.dto.MetaDataResult;
import de.chrikra.dto.MetaDataResultList;
import de.chrikra.dto.MetricDescription;
import de.chrikra.dto.SeriesResult;
import de.chrikra.dto.SeriesResultEntry;
import de.chrikra.dto.TargetEntry;
import de.chrikra.dto.TargetMetaDataEntry;
import de.chrikra.dto.TargetMetaDataResult;
import de.chrikra.dto.TargetResult;
import de.chrikra.dto.TargetResultData;
import de.chrikra.user.UserService;
import de.chrikra.util.MultiPredicate;

@ApplicationScoped
public class ResultFilterService {

	@Inject
	UserService userService;

	public Response filterSeriesResult(Response resp, String user) {
		List<Predicate<SeriesResultEntry>> filter = userService.getSeriesResultFilter(user);
		if (CollectionUtils.isEmpty(filter)) {
			return resp;
		}

		if (resp.getStatus() != HttpStatus.SC_OK) {
			return resp;
		}

		SeriesResult result = resp.readEntity(SeriesResult.class);
		List<SeriesResultEntry> data = result.getData();
		List<SeriesResultEntry> filteredData = data.stream().filter(new MultiPredicate<>(filter))
				.collect(Collectors.toList());
		result.setData(filteredData);
		return createResponseCopyWithNewEntity(resp, result);
	}

	private Response createResponseCopyWithNewEntity(Response resp, Object entity) {
		ResponseBuilder builder = Response.status(resp.getStatus()).entity(entity).type(resp.getMediaType());
		resp.close();
		return builder.build();
	}

	public Response filterTargetResults(Response resp, String user) {
		List<Predicate<TargetEntry>> targetResultFilter = userService.getTargetResultFilter(user);
		if (CollectionUtils.isEmpty(targetResultFilter)) {
			return resp;
		}

		if (resp.getStatus() != HttpStatus.SC_OK) {
			return resp;
		}

		TargetResult targetResult = resp.readEntity(TargetResult.class);
		TargetResultData data = targetResult.getData();
		List<TargetEntry> activeTargets = data.getActiveTargets();
		if (!CollectionUtils.isEmpty(activeTargets)) {
			List<TargetEntry> filteredActiveTargets = activeTargets.stream()
					.filter(new MultiPredicate<>(targetResultFilter)).collect(Collectors.toList());
			data.setActiveTargets(filteredActiveTargets);
		}

		List<TargetEntry> droppedTargets = data.getDroppedTargets();
		if (!CollectionUtils.isEmpty(droppedTargets)) {
			List<TargetEntry> filteredDroppedTargets = droppedTargets.stream()
					.filter(new MultiPredicate<>(targetResultFilter)).collect(Collectors.toList());
			data.setDroppedTargets(filteredDroppedTargets);
		}
		return createResponseCopyWithNewEntity(resp, targetResult);
	}

	public Response filterLabelValues(Response resp, String labelName, String user) {
		List<Predicate<String>> labelValueFilter = userService.getLabelValueFilter(user, labelName);
		if (CollectionUtils.isEmpty(labelValueFilter)) {
			return resp;
		}

		if (resp.getStatus() != HttpStatus.SC_OK) {
			return resp;
		}

		LabelValueResult labelValues = resp.readEntity(LabelValueResult.class);
		List<String> data = labelValues.getData();
		List<String> filteredData = data.stream().filter(new MultiPredicate<>(labelValueFilter))
				.collect(Collectors.toList());
		labelValues.setData(filteredData);

		return createResponseCopyWithNewEntity(resp, labelValues);
	}

	public Response filterTargetMetaDataResult(Response resp, String user) {
		List<Predicate<TargetMetaDataEntry>> filter = userService.getTargetMetaDataFilter(user);
		if (CollectionUtils.isEmpty(filter)) {
			return resp;
		}

		if (resp.getStatus() != HttpStatus.SC_OK) {
			return resp;
		}

		TargetMetaDataResult result = resp.readEntity(TargetMetaDataResult.class);
		List<TargetMetaDataEntry> data = result.getData();
		List<TargetMetaDataEntry> filteredData = data.stream().filter(new MultiPredicate<>(filter))
				.collect(Collectors.toList());
		result.setData(filteredData);
		return createResponseCopyWithNewEntity(resp, result);
	}

	public Response filterMetaDataResult(Response resp, String user) {
		List<Predicate<MetaDataEntry>> filter = userService.getMetaDataFilter(user);
		if (CollectionUtils.isEmpty(filter)) {
			return resp;
		}

		if (resp.getStatus() != HttpStatus.SC_OK) {
			return resp;
		}
		
		MetaDataResult res = resp.readEntity(MetaDataResult.class);
		MetaDataResultList data = res.getData();
		List<MetaDataEntry> data2 = data.getData();
		List<MetaDataEntry> filteredList = data2.stream().filter(new MultiPredicate<>(filter)).collect(Collectors.toList());
		data.setData(filteredList);
		
		return createResponseCopyWithNewEntity(resp, res);
	}

}
