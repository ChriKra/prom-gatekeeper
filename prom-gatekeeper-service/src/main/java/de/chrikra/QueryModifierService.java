package de.chrikra;

import java.io.IOException;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import de.chrikra.query.PromQLParser;
import de.chrikra.query.QueryElement;
import de.chrikra.query.modifier.QueryModificationException;
import de.chrikra.query.modifier.QueryModifier;
import de.chrikra.user.UserService;

@ApplicationScoped
public class QueryModifierService {

	@Inject
	UserService userService;

	public String modifyQuery(String query, String user) throws IOException, QueryModificationException {
		QueryElement qe = parseQuery(query);
		List<QueryModifier> modifiers = userService.getConfiguredQueryModifier(user);

		for (QueryModifier modifier : modifiers) {
			qe.apply(modifier);
		}

		return qe.toString();
	}

	private QueryElement parseQuery(String query) throws IOException {
		return new PromQLParser().parse(query);
	}
}
