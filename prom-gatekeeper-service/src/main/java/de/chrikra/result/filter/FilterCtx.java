package de.chrikra.result.filter;

import de.chrikra.MetaDataCache;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class FilterCtx {
	private MetaDataCache cache;
}
