package de.chrikra.result.filter;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LabelValueFilter implements Predicate<String> {

	private Collection<String> allowedValues;

	@Override
	public boolean test(String arg0) {
		return allowedValues.contains(arg0);
	}
	
	public static LabelValueFilter createAllowedJobsByInstancesFilter(FilterCtx ctx, List<String> allowedInstances) {
		return new LabelValueFilter(ctx.getCache().getJobsForInstances(allowedInstances));
	}
	
	public static LabelValueFilter createAllowedInstancesByJobsFilter(FilterCtx ctx, List<String> allowedJobs) {
		return new LabelValueFilter(ctx.getCache().getInstancesForJobs(allowedJobs));
	}
}
