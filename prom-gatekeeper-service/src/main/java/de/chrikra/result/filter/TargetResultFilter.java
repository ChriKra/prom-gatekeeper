package de.chrikra.result.filter;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import de.chrikra.dto.TargetEntry;

public class TargetResultFilter implements Predicate<TargetEntry> {
	private static final int FILTER_BY_INSTANCE = 1;
	private static final int FILTER_BY_JOB = 2;
	private List<String> allowedValues;
	private int filterMode;

	private TargetResultFilter() {

	}

	public static TargetResultFilter createFilterByInstance(List<String> allowedInstances) {
		TargetResultFilter res = new TargetResultFilter();
		res.allowedValues = allowedInstances;
		res.filterMode = FILTER_BY_INSTANCE;
		return res;
	}

	@Override
	public boolean test(TargetEntry arg0) {
		switch (filterMode) {
		case FILTER_BY_INSTANCE:
			return filterByInstance(arg0);
		case FILTER_BY_JOB:
			return filterByJob(arg0);
		}
		return false;
	}

	private boolean filterByJob(TargetEntry arg0) {
		Map<String, String> labels = arg0.getLabels();
		String job = labels.get("job");

		return allowedValues.contains(job);
	}

	private boolean filterByInstance(TargetEntry arg0) {
		Map<String, String> labels = arg0.getLabels();
		String instance = labels.get("instance");

		return allowedValues.contains(instance);
	}

	public static Predicate<TargetEntry> createFilterByJob(List<String> allowedJobs) {
		TargetResultFilter res = new TargetResultFilter();
		res.allowedValues = allowedJobs;
		res.filterMode = FILTER_BY_JOB;
		return res;
	}

}
