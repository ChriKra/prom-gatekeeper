package de.chrikra.result.filter;

import java.util.function.Predicate;

@FunctionalInterface
public interface FilterProducer<T> {
	Predicate<T> createFilter(FilterCtx ctx);
}
