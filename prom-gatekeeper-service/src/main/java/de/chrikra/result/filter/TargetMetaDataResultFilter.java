package de.chrikra.result.filter;

import java.util.List;
import java.util.function.Predicate;

import de.chrikra.dto.TargetMetaDataEntry;

public class TargetMetaDataResultFilter implements Predicate<TargetMetaDataEntry> {
	private static final int FILTER_BY_INSTANCE = 1;
	private static final int FILTER_BY_JOB = 2;
	private List<String> allowedValues;
	private int filterMode;

	private TargetMetaDataResultFilter() {

	}

	public static TargetMetaDataResultFilter createFilterByInstance(List<String> allowedInstances) {
		TargetMetaDataResultFilter res = new TargetMetaDataResultFilter();
		res.allowedValues = allowedInstances;
		res.filterMode = FILTER_BY_INSTANCE;
		return res;
	}

	@Override
	public boolean test(TargetMetaDataEntry targetMetaDataEntry) {
		switch (filterMode) {
		case FILTER_BY_INSTANCE:
			return allowedValues.contains(targetMetaDataEntry.getTarget().getInstance());
		case FILTER_BY_JOB:
			return allowedValues.contains(targetMetaDataEntry.getTarget().getJob());
		}
		
		return false;
	}

	public static Predicate<TargetMetaDataEntry> createFilterByJob(List<String> allowedJobs) {
		TargetMetaDataResultFilter res = new TargetMetaDataResultFilter();
		res.allowedValues = allowedJobs;
		res.filterMode = FILTER_BY_JOB;
		return res;
	}
}
