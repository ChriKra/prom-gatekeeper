package de.chrikra.result.filter;

import java.util.function.Predicate;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class StaticFilterBuilder<T> implements FilterProducer<T> {

	private Predicate<T> instance;

	@Override
	public Predicate<T> createFilter(FilterCtx ctx) {
		return instance;
	}

}
