package de.chrikra.result.filter;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import de.chrikra.dto.MetaDataEntry;

public class MetaDataFilter implements Predicate<MetaDataEntry>{

	private Set<String> allowedMetrics;

	public MetaDataFilter(Set<String> allowedMetrics) {
		this.allowedMetrics = allowedMetrics;
	}
	
	@Override
	public boolean test(MetaDataEntry arg0) {
		return allowedMetrics.contains(arg0.getMetric());
	}
	
	public static MetaDataFilter createAllowedInstancesFilter(FilterCtx ctx, List<String> allowedInstances) {
		return new MetaDataFilter(ctx.getCache().getMetricsForInstances(allowedInstances));
	}

	public static MetaDataFilter createAllowedJobsFilter(FilterCtx ctx, List<String> allowedJobs) {
		return new MetaDataFilter(ctx.getCache().getMetricsForJobs(allowedJobs));
	}
}
