package de.chrikra.result.filter;

import java.util.List;
import java.util.function.Predicate;

import de.chrikra.dto.SeriesResultEntry;

public class SeriesResultFilter implements Predicate<SeriesResultEntry> {
	private static final int FILTER_BY_INSTANCE = 1;
	private static final int FILTER_BY_JOB = 2;
	private List<String> allowedValues;
	private int filterMode;

	private SeriesResultFilter() {

	}

	public static SeriesResultFilter createFilterByInstance(List<String> allowedInstances) {
		SeriesResultFilter res = new SeriesResultFilter();
		res.allowedValues = allowedInstances;
		res.filterMode = FILTER_BY_INSTANCE;
		return res;
	}
	
	public static SeriesResultFilter createFilterByJob(List<String> allowedInstances) {
		SeriesResultFilter res = new SeriesResultFilter();
		res.allowedValues = allowedInstances;
		res.filterMode = FILTER_BY_JOB;
		return res;
	}

	@Override
	public boolean test(SeriesResultEntry seriesResultEntry) {
		switch (filterMode) {
		case FILTER_BY_INSTANCE:
			return allowedValues.contains(seriesResultEntry.getInstance());
		case FILTER_BY_JOB:
			return allowedValues.contains(seriesResultEntry.getJob());
		}
		return false;
	}
}
