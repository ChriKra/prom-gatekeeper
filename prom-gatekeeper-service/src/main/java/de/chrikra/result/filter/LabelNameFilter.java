package de.chrikra.result.filter;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

public class LabelNameFilter implements Predicate<String> {

	private Set<String> allowedMetrics;

	public LabelNameFilter(Set<String> allowedMetrics) {
		this.allowedMetrics = allowedMetrics;
	}

	@Override
	public boolean test(String arg0) {
		return this.allowedMetrics.contains(arg0);
	}

	public static LabelNameFilter createAllowedInstancesLabelFilter(FilterCtx ctx, List<String> allowedInstances) {
		return new LabelNameFilter(ctx.getCache().getMetricsForInstances(allowedInstances));
	}

	public static LabelNameFilter createAllowedJobsLabelFilter(FilterCtx ctx, List<String> allowedJobs) {
		return new LabelNameFilter(ctx.getCache().getMetricsForJobs(allowedJobs));
	}

}
