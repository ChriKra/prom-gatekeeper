package de.chrikra;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Path;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey = "prometheus-api")
@ApplicationScoped
@Path("/api/v1")
public interface PrometheusClient extends PrometheusServiceDefinition{

}
