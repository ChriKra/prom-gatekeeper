package de.chrikra;

import java.io.IOException;
import java.util.function.Function;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import de.chrikra.query.modifier.EmptyResultException;
import lombok.extern.slf4j.Slf4j;

@RequestScoped
@Slf4j
public class PromGateKeeperService {

	@Inject
	@RestClient
	PrometheusClient prometheusClient;

	@Inject
	QueryModifierService queryModifier;

	@Inject
	ResultFilterService resultFilter;
	

	public Response query(String query, String time, String timeout, boolean doPost, String  user) {
		if (doPost) {
			return modifyQueryAndSubmit(query, x -> prometheusClient.queryPost(x, time, timeout), user);
		} else {
			return modifyQueryAndSubmit(query, x -> prometheusClient.query(x, time, timeout), user);
		}
	}

	public Response queryRange(String query, String start, String end, String step, String timeout, boolean doPost, String user) {
		if (doPost) {
			return modifyQueryAndSubmit(query, x -> prometheusClient.queryRangePost(x, start, end, step, timeout), user);
		} else {
			return modifyQueryAndSubmit(query, x -> prometheusClient.queryRange(x, start, end, step, timeout), user);
		}
	}

	public Response series(String match, String start, String end, boolean doPost, String user) {
		Response resp = null;
		if (doPost) {
			resp = prometheusClient.seriesPost(match, start, end);
		} else {
			resp = prometheusClient.series(match, start, end);
		}

		resp = resultFilter.filterSeriesResult(resp, user);
		return resp;
	}

	public Response labels(boolean doPost) {
		Response resp = null;
		if (doPost) {
			resp = prometheusClient.labelsPost();
		} else {
			resp = prometheusClient.labels();
		}

		return resp;
	}

	public Response labelValues(String labelName, String user) {
		Response resp = prometheusClient.labelValues(labelName);
		resp = resultFilter.filterLabelValues(resp, labelName, user);
		return resp;
	}

	public Response targets(String state, String user) {
		Response resp = prometheusClient.targets(state);
		resp = resultFilter.filterTargetResults(resp, user);
		return resp;
	}

	public Response rules(String type) {
		Response resp = prometheusClient.rules(type);
		return resp;
	}

	public Response alerts() {
		Response resp = prometheusClient.alerts();
		return resp;
	}

	private Response modifyQueryAndSubmit(String query, Function<String, Response> f, String user) {
		String _query = null;
		try {
			_query = queryModifier.modifyQuery(query, user);
		} catch (IOException e) {
			log.error("Can't read query string", e);
			return Response.status(422).build();
		} catch (EmptyResultException e) {
			return e.getResponse();
		}

		return f.apply(_query);
	}

	public Response targetMetaData(String matchTarget, String metric, Integer limit, String user) {
		Response resp = prometheusClient.targetMetaData(matchTarget, metric, limit);
		resp = resultFilter.filterTargetMetaDataResult(resp, user);
		return resp;
	}

	public Response metaData(String metric, Integer limit, String user) {
		Response resp = prometheusClient.metaData( limit, metric);
		resp = resultFilter.filterMetaDataResult(resp, user);
		return resp;
	}

}
