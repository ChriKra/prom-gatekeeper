package de.chrikra.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import de.chrikra.dto.MetaDataEntry;
import de.chrikra.dto.MetaDataResultList;
import de.chrikra.dto.MetricDescription;

public class MetaDataResultListDeserializer extends JsonDeserializer<MetaDataResultList>{

	@Override
	public MetaDataResultList deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		ObjectCodec codec = p.getCodec();
		TreeNode node = codec.readTree(p);
		 List<MetaDataEntry> data = new ArrayList<MetaDataEntry>();
		Iterator<String> fieldNames = node.fieldNames();
		while(fieldNames.hasNext()) {
			String fieldName = fieldNames.next();
			MetaDataEntry entry = new MetaDataEntry();
			entry.setMetric(fieldName);
			data.add(entry);
			ArrayNode descriptions = (ArrayNode) node.get(fieldName);
			
//			Iterator<MetricDescription> readValuesAs = descriptions.traverse(p.getCodec()).readValuesAs(MetricDescription.class);
			List<MetricDescription> descriptionList = new ArrayList<MetricDescription>();
			entry.setDescriptions(descriptionList);
//			readValuesAs.forEachRemaining(descriptionList::add);
			
			
			Iterator<JsonNode> descriptionsIter = descriptions.iterator();
			while(descriptionsIter.hasNext()) {
				JsonNode descriptionNode = descriptionsIter.next();
				MetricDescription metricDescription = descriptionNode.traverse(codec).readValueAs(MetricDescription.class);
				descriptionList.add(metricDescription);
			}

		}
		MetaDataResultList res = new MetaDataResultList();
		res.setData(data);
		return res;
	}
	
}