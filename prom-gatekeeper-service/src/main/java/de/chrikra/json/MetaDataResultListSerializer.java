package de.chrikra.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import de.chrikra.dto.MetaDataEntry;
import de.chrikra.dto.MetaDataResultList;
import de.chrikra.dto.MetricDescription;

public class MetaDataResultListSerializer extends JsonSerializer<MetaDataResultList> {

	@Override
	public void serialize(MetaDataResultList value, JsonGenerator gen, SerializerProvider serializers)
			throws IOException {
		gen.writeStartObject();
		for (MetaDataEntry s : value.getData()) {

			gen.writeArrayFieldStart(s.getMetric());
			for (MetricDescription d : s.getDescriptions()) {
				gen.writeObject(d);
			}
			gen.writeEndArray();
		}
		gen.writeEndObject();
	}

}