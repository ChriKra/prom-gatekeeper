package de.chrikra.util;

import java.util.List;
import java.util.function.Predicate;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MultiPredicate<T> implements Predicate<T>{
	public List<? extends Predicate<T>> predicates;

	
	public boolean test(T testee) {
		boolean result = true;
		for (Predicate<T> predicate : predicates) {
			result &= predicate.test(testee);
		}
		return result;
	}
}