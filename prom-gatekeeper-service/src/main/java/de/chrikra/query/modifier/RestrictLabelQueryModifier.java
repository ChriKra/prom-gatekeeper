package de.chrikra.query.modifier;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Predicate;
import com.google.re2j.Pattern;

import de.chrikra.query.LabelMatcher;
import de.chrikra.query.VectorSelector;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Slf4j
public class RestrictLabelQueryModifier extends BaseModifier {
	private String label;
	private List<String> allowedLabelValues;

	@Override
	public void modify(VectorSelector vectorSelector) throws QueryModificationException {
		List<LabelMatcher> labelMatcher = vectorSelector.getLabelMatcher();
		if (CollectionUtils.isEmpty(labelMatcher)) {
			labelMatcher = new ArrayList<>();
			vectorSelector.setLabelMatcher(labelMatcher);
		}

		LabelMatcher matcher = IterableUtils.find(labelMatcher, x -> x.getIdentifier().equals(this.label));
		// ToDo: Maybe more matchers are allowed?!
		if (matcher == null) {
			matcher = createLabelMatcher();
			labelMatcher.add(matcher);
		} else {
			modifyMatcher(matcher);
		}
	}

	private void modifyMatcher(LabelMatcher matcher) throws EmptyResultException {
		if (StringUtils.isEmpty(matcher.getValue())) {
			LabelMatcher createLabelMatcher = createLabelMatcher();
			matcher.setOp(createLabelMatcher.getOp());
			matcher.setValue(createLabelMatcher.getValue());
			return;
		}

		switch (matcher.getOp()) {
		case "=":
			modifyEqualMatcher(matcher);
			break;
		case "!=":
			modifyNotEqualMatcher(matcher);
			break;
		case "=~":
			modifyRegexMatcher(matcher);
			break;
		case "!~":
			modifyNotRegexMatcher(matcher);
			break;
		default:
			throw new QueryModificationException("Unsupport operator in label matcher:" + matcher.getOp());
		}
	}

	private void modifyNotRegexMatcher(LabelMatcher matcher) {
		Pattern p = Pattern.compile(matcher.getValue());
		rewriteMatcherWithAllowedValues(matcher, x -> !p.matches(x));
	}

	private void modifyRegexMatcher(LabelMatcher matcher) {
		Pattern p = Pattern.compile(matcher.getValue());
		rewriteMatcherWithAllowedValues(matcher, x -> p.matches(x));
	}

	private void modifyNotEqualMatcher(LabelMatcher matcher) {
		rewriteMatcherWithAllowedValues(matcher, x -> !x.equals(matcher.getValue()));
	}

	private void modifyEqualMatcher(LabelMatcher matcher) {
		// If the matcher does not contain any of the allowed label values
		if (!allowedLabelValues.contains(matcher.getValue())) {
			// ... throw an exception leading to an empty result
			throw new EmptyResultException("vector");
		}
	}

	private void rewriteMatcherWithAllowedValues(LabelMatcher matcher, Predicate<String> valueMatcher) {
		List<String> labelValues = this.allowedLabelValues.stream().filter(valueMatcher).collect(Collectors.toList());
		if (labelValues.isEmpty()) {
			throw new EmptyResultException("vector");
		}
		setLabelValues(matcher, labelValues);
	}

	private LabelMatcher createLabelMatcher() {
		LabelMatcher matcher = new LabelMatcher();
		matcher.setIdentifier(label);
		setLabelValues(matcher, allowedLabelValues);
		return matcher;
	}

	private void setLabelValues(LabelMatcher matcher, List<String> values) {
		if (values.size() > 1) {
			matcher.setOp("=~");
			matcher.setValue(StringUtils.join(values, '|'));
		} else {
			matcher.setOp("=");
			matcher.setValue(values.get(0));
		}
	}
}
