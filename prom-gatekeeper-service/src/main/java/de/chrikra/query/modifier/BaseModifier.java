package de.chrikra.query.modifier;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import de.chrikra.query.AggregateExpression;
import de.chrikra.query.BinaryExpression;
import de.chrikra.query.FunctionCall;
import de.chrikra.query.MatrixSelector;
import de.chrikra.query.NumberLiteral;
import de.chrikra.query.OffsetExpression;
import de.chrikra.query.ParenExpression;
import de.chrikra.query.QueryElement;
import de.chrikra.query.QueryExpression;
import de.chrikra.query.StringLiteral;
import de.chrikra.query.SubQuery;
import de.chrikra.query.UnaryExpression;
import de.chrikra.query.VectorSelector;

/**
 * Base class for implementations of {@linkplain QueryModifier}
 * @author christoph
 *
 */
public class BaseModifier implements QueryModifier{

	@Override
	public void modify(FunctionCall functionCall) throws QueryModificationException {
		call(functionCall.getBody());
	}

	@Override
	public void modify(AggregateExpression aggregateExpression) throws QueryModificationException {
		call(aggregateExpression.getBody());
	}

	@Override
	public void modify(VectorSelector vectorSelector) throws QueryModificationException {
		//do nothing
	}

	@Override
	public void modify(UnaryExpression unaryExpression) throws QueryModificationException {
		QueryExpression expr = unaryExpression.getExpr();
		if(expr != null) {
			expr.apply(this);
		}
	}

	@Override
	public void modify(SubQuery subQuery) throws QueryModificationException {
		QueryExpression expr = subQuery.getExpression();
		if(expr != null) {
			expr.apply(this);
		}
	}

	@Override
	public void modify(StringLiteral stringLiteral) throws QueryModificationException {
		//do nothing
	}

	@Override
	public void modify(ParenExpression parenExpression) throws QueryModificationException {
		QueryExpression expr = parenExpression.getExpression();
		if(expr != null) {
			expr.apply(this);
		}
	}

	@Override
	public void modify(NumberLiteral numberLiteral) throws QueryModificationException {
		//do nothing
	}

	@Override
	public void modify(MatrixSelector matrixSelector) throws QueryModificationException {
		QueryExpression expr = matrixSelector.getExpression();
		if(expr != null) {
			expr.apply(this);
		}
	}

	@Override
	public void modify(BinaryExpression binaryExpression) {
		QueryExpression expression = binaryExpression.getLhs();
		if (expression != null) {
			expression.apply(this);
		}
		
		expression = binaryExpression.getRhs();
		if (expression != null) {
			expression.apply(this);
		}
	}

	@Override
	public void modify(OffsetExpression offsetExpression) {
		QueryExpression expression = offsetExpression.getExpression();
		if (expression != null) {
			expression.apply(this);
		}
	}

	private void call(List<? extends QueryElement> elements) throws QueryModificationException {
		if (CollectionUtils.isEmpty(elements)) {
			return;
		}
		for (QueryElement qe : elements) {
			qe.apply(this);
		}
	}
}
