package de.chrikra.query.modifier;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.chrikra.prom.PrometheusBehaviour;
import lombok.Getter;

@Getter
public class EmptyResultException extends QueryModificationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -803419535262296790L;

	private String resultType;

	public EmptyResultException(String resultType) {
		this.resultType = resultType;
	}

	
	public Response getResponse() {
		return Response.status(200).entity(PrometheusBehaviour.emptyQueryResult(resultType))
				.type(MediaType.APPLICATION_JSON).build();
	}

}
