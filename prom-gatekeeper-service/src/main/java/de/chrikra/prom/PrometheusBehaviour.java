package de.chrikra.prom;

public class PrometheusBehaviour {

	public static QueryResult emptyQueryResult(String type) {
		QueryResult res = new QueryResult();
		res.setStatus("success");
		QueryData data = new QueryData();
		res.setData(data);
		data.setResultType(type);
		return res;
	}
}
