package de.chrikra.prom;

import lombok.Data;

@Data
public class QueryResult {
	private String status;
	private QueryData data;
}
