package de.chrikra.prom;

import java.util.List;

import lombok.Data;

@Data
public class QueryData {
	private String resultType;
	private List<String> result;
}
