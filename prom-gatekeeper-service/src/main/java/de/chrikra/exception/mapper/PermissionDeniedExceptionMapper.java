package de.chrikra.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import de.chrikra.exception.PermissionDeniedException;

@Provider
public class PermissionDeniedExceptionMapper implements ExceptionMapper<PermissionDeniedException> {

	@Override
	public Response toResponse(PermissionDeniedException exception) {
		return Response.status(Response.Status.FORBIDDEN).entity("Access to the resource is forbidden").build();
	}

}
