package de.chrikra.user;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.chrikra.dto.MetaDataEntry;
import de.chrikra.dto.SeriesResultEntry;
import de.chrikra.dto.TargetEntry;
import de.chrikra.dto.TargetMetaDataEntry;
import de.chrikra.query.modifier.QueryModifier;
import de.chrikra.query.modifier.RestrictLabelQueryModifier;
import de.chrikra.result.filter.FilterProducer;
import de.chrikra.result.filter.LabelNameFilter;
import de.chrikra.result.filter.LabelValueFilter;
import de.chrikra.result.filter.MetaDataFilter;
import de.chrikra.result.filter.SeriesResultFilter;
import de.chrikra.result.filter.TargetMetaDataResultFilter;
import de.chrikra.result.filter.TargetResultFilter;
import io.quarkus.runtime.configuration.ConfigurationException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonUserAccessDefinitionParser {

	public Map<String, UserSetting> parse(String path) {
		log.info("Loading config from '{}'", path);
		try (FileInputStream fis = new FileInputStream(path)) {
			return parse(fis);
		} catch (IOException e) {
			throw new ConfigurationException("Can't parse config", e);
		}
	}

	public Map<String, UserSetting> parseResource(String resource) {
		log.info("Loading config from resource '{}'", resource);
		try (InputStream is = PropertyFileUserService.class.getResourceAsStream(resource)) {
			return parse(is);
		} catch (IOException e) {
			throw new ConfigurationException("Can't parse config", e);
		}
	}

	public Map<String, UserSetting> parse(InputStream fis) {
		try {
			UserAccessDefintions config = readConfig(fis);
			return process(config);
		} catch (IOException e) {
			throw new ConfigurationException("Can't parse config", e);
		}
	}

	private Map<String, UserSetting> process(UserAccessDefintions config) {
		Map<String, UserSetting> result = new HashMap<>();
		List<UserAccessDefinition> uads = config.getUads();
		if (uads != null) {
			uads.forEach(x -> process(x, result));
		}
		return result;
	}

	private void process(UserAccessDefinition uad, Map<String, UserSetting> result) {
		UserSetting setting = new UserSetting();
		setting.setQueryModifier(createQueryModifier(uad));
		setting.setSeriesResultFilter(createSeriesResultFilter(uad));
		setting.setTargetResultFilter(createTargetResultFilter(uad));
		setting.setLabelValueFilter(createLabelValueFilter(uad));
		setting.setTargetMetaDataResultFilter(createTargetMetaDataFilter(uad));
		setting.setMetaDataResultFilter(createMetaDataFilter(uad));
		setting.setResourceAccessPermissions(loadResourceAccessPermissions(uad));
		result.put(uad.getUsername(), setting);
	}

	private List<FilterProducer<MetaDataEntry>> createMetaDataFilter(UserAccessDefinition uad) {
		List<String> allowedInstances = uad.getAllowedInstances();
		List<FilterProducer<MetaDataEntry>> res = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(allowedInstances)) {
			res.add(x -> MetaDataFilter.createAllowedInstancesFilter(x, allowedInstances));
		}

		List<String> allowedJobs = uad.getAllowedJobs();
		if (CollectionUtils.isNotEmpty(allowedJobs)) {
			res.add(x -> MetaDataFilter.createAllowedJobsFilter(x, allowedJobs));
		}

		return res;
	}

	private UserResourceAccessPermission loadResourceAccessPermissions(UserAccessDefinition uad) {
		UserResourceAccessPermission res = uad.getResourceAccess();
		if (res == null) {
			log.info(
					"User access definition for user {} does not contain resource access permissions. Will use default permissions!",
					uad.getUsername());
			res = new UserResourceAccessPermission();
		}
		return res;
	}

	private List<Predicate<TargetMetaDataEntry>> createTargetMetaDataFilter(UserAccessDefinition uad) {
		List<String> allowedInstances = uad.getAllowedInstances();
		List<Predicate<TargetMetaDataEntry>> filter = new ArrayList<>();
		if (!CollectionUtils.isEmpty(allowedInstances)) {
			filter.add(TargetMetaDataResultFilter.createFilterByInstance(allowedInstances));
		}

		List<String> allowedJobs = uad.getAllowedJobs();
		if (!CollectionUtils.isEmpty(allowedJobs)) {
			filter.add(TargetMetaDataResultFilter.createFilterByJob(allowedJobs));
		}
		return filter;
	}

	private Map<String, List<FilterProducer<String>>> createLabelValueFilter(UserAccessDefinition uad) {
		List<String> allowedInstances = uad.getAllowedInstances();
		Map<String, List<FilterProducer<String>>> map = new HashMap<>();
		List<FilterProducer<String>> instanceFilter = new ArrayList<>();
		List<FilterProducer<String>> nameFilter = new ArrayList<>();
		List<FilterProducer<String>> jobFilter = new ArrayList<>();
		if (!CollectionUtils.isEmpty(allowedInstances)) {
			instanceFilter.add(x -> new LabelValueFilter(allowedInstances));
			nameFilter.add(x -> LabelNameFilter.createAllowedInstancesLabelFilter(x, allowedInstances));
			jobFilter.add(x -> LabelValueFilter.createAllowedJobsByInstancesFilter(x, allowedInstances));
		}

		List<String> allowedJobs = uad.getAllowedJobs();
		if (!CollectionUtils.isEmpty(allowedJobs)) {
			jobFilter.add(x -> new LabelValueFilter(allowedJobs));
			nameFilter.add(x -> LabelNameFilter.createAllowedJobsLabelFilter(x, allowedJobs));
			instanceFilter.add(x -> LabelValueFilter.createAllowedInstancesByJobsFilter(x, allowedJobs));
		}

		if (!nameFilter.isEmpty()) {
			map.put("__name__", nameFilter);
		}
		
		if(!instanceFilter.isEmpty()) {
			map.put("instance", instanceFilter);
		}
		
		if(!jobFilter.isEmpty()) {
			map.put("job", jobFilter);
		}

		return map;
	}

	private List<Predicate<TargetEntry>> createTargetResultFilter(UserAccessDefinition uad) {
		List<String> allowedInstances = uad.getAllowedInstances();
		List<Predicate<TargetEntry>> filter = new ArrayList<>();
		if (!CollectionUtils.isEmpty(allowedInstances)) {
			filter.add(TargetResultFilter.createFilterByInstance(allowedInstances));
		}

		List<String> allowedJobs = uad.getAllowedJobs();
		if (!CollectionUtils.isEmpty(allowedJobs)) {
			filter.add(TargetResultFilter.createFilterByJob(allowedJobs));
		}
		return filter;
	}

	private List<Predicate<SeriesResultEntry>> createSeriesResultFilter(UserAccessDefinition uad) {
		List<String> allowedInstances = uad.getAllowedInstances();
		List<Predicate<SeriesResultEntry>> filter = new ArrayList<>();
		if (!CollectionUtils.isEmpty(allowedInstances)) {
			filter.add(SeriesResultFilter.createFilterByInstance(allowedInstances));
		}

		List<String> allowedJobs = uad.getAllowedJobs();
		if (!CollectionUtils.isEmpty(allowedJobs)) {
			filter.add(SeriesResultFilter.createFilterByJob(allowedJobs));
		}
		return filter;
	}

	private List<QueryModifier> createQueryModifier(UserAccessDefinition uad) {
		List<String> allowedInstances = uad.getAllowedInstances();
		List<QueryModifier> queryModifier = new ArrayList<>();
		if (!CollectionUtils.isEmpty(allowedInstances)) {
			queryModifier.add(new RestrictLabelQueryModifier("instance", allowedInstances));
		}

		List<String> allowedJobs = uad.getAllowedJobs();
		if (!CollectionUtils.isEmpty(allowedJobs)) {
			queryModifier.add(new RestrictLabelQueryModifier("job", allowedJobs));
		}

		return queryModifier;
	}

	private UserAccessDefintions readConfig(InputStream is) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(is, UserAccessDefintions.class);
	}
}
