package de.chrikra.user;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import de.chrikra.dto.MetaDataEntry;
import de.chrikra.dto.SeriesResultEntry;
import de.chrikra.dto.TargetEntry;
import de.chrikra.dto.TargetMetaDataEntry;
import de.chrikra.query.modifier.QueryModifier;
import de.chrikra.result.filter.FilterProducer;
import lombok.Data;

@Data
public class UserSetting {
	private List<QueryModifier> queryModifier;
	private List<Predicate<SeriesResultEntry>> seriesResultFilter;
	private List<Predicate<TargetEntry>> targetResultFilter;
	private Map<String, List<FilterProducer<String>>> labelValueFilter;
	private List<Predicate<TargetMetaDataEntry>> targetMetaDataResultFilter;
	private List<FilterProducer<MetaDataEntry>> metaDataResultFilter;
	private UserResourceAccessPermission resourceAccessPermissions; 
}
