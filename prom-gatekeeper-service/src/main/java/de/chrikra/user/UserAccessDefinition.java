package de.chrikra.user;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserAccessDefinition {
	private String username;
	private List<String> allowedInstances;
	private List<String> allowedJobs;
	private UserResourceAccessPermission resourceAccess;
}
