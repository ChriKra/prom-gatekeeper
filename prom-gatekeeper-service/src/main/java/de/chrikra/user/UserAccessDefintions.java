package de.chrikra.user;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserAccessDefintions {
	private List<UserAccessDefinition> uads;
}
