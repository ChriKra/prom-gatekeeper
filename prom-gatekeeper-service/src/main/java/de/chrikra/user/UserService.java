package de.chrikra.user;

import java.util.List;
import java.util.function.Predicate;

import de.chrikra.dto.MetaDataEntry;
import de.chrikra.dto.SeriesResultEntry;
import de.chrikra.dto.TargetEntry;
import de.chrikra.dto.TargetMetaDataEntry;
import de.chrikra.query.modifier.QueryModifier;

public interface UserService {

	List<QueryModifier> getConfiguredQueryModifier(String user);

	List<Predicate<SeriesResultEntry>> getSeriesResultFilter(String user);
	
	List<Predicate<TargetEntry>> getTargetResultFilter(String user);

	List<Predicate<String>> getLabelValueFilter(String user, String labelName);

	List<Predicate<TargetMetaDataEntry>> getTargetMetaDataFilter(String user);

	void checkQueryPermission(String user, boolean rangeQuery);

	void checkSeriesPermission(String user);

	void checkLabelsPermission(String user);

	void checkLabelValuesPermission(String user);

	void checkTargetsPermission(String user);

	void checkRulesPermission(String user);

	void checkAlertsPermission(String user);

	void checkTargetMetaDataPermission(String user);

	void checkMetaDataPermission(String user);

	List<Predicate<MetaDataEntry>> getMetaDataFilter(String user);
}
