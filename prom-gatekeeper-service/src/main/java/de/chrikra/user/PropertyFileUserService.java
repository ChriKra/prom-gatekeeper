package de.chrikra.user;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.chrikra.MetaDataCache;
import de.chrikra.dto.MetaDataEntry;
import de.chrikra.dto.SeriesResultEntry;
import de.chrikra.dto.TargetEntry;
import de.chrikra.dto.TargetMetaDataEntry;
import de.chrikra.exception.PermissionDeniedException;
import de.chrikra.query.modifier.QueryModifier;
import de.chrikra.result.filter.FilterCtx;
import de.chrikra.result.filter.FilterProducer;
import io.quarkus.runtime.StartupEvent;

@ApplicationScoped
public class PropertyFileUserService implements UserService {
	private static final Logger LOG = LoggerFactory.getLogger(PropertyFileUserService.class);

	@ConfigProperty(name = "promgatekeeper.user.access.definition")
	Optional<String> configFile;

	@Inject
	private MetaDataCache metaDataCache;

	private Map<String, UserSetting> userSettings;

	private FilterCtx filterCtx;

	@PostConstruct
	public void loadConfig() {
		this.filterCtx = new FilterCtx(metaDataCache);
		this.userSettings = configFile.map(this::loadFromExternalFile).orElseGet(this::loadDefault);

		if (this.userSettings == null) {
			LOG.warn("No user access definition provided!");
			return;
		}
	}

	private Map<String, UserSetting> loadFromExternalFile(String path) {
		LOG.info("Loading config from '{}'", path);
		JsonUserAccessDefinitionParser parser = new JsonUserAccessDefinitionParser();
		return parser.parse(path);

	}

	private Map<String, UserSetting> loadDefault() {
		LOG.info("Loading default config");
		JsonUserAccessDefinitionParser parser = new JsonUserAccessDefinitionParser();
		return parser.parseResource("/defaultUserAccess.json");
	}

	/**
	 * Observe the {@linkplain StartupEvent} to make this bean instantiated on
	 * application start
	 * 
	 * @param event
	 */
	void startup(@Observes StartupEvent event) {
	}

	@Override
	public List<QueryModifier> getConfiguredQueryModifier(String user) {
		return getList(user, UserSetting::getQueryModifier);
	}

	@Override
	public List<Predicate<SeriesResultEntry>> getSeriesResultFilter(String user) {
		return getList(user, UserSetting::getSeriesResultFilter);
	}

	@Override
	public List<Predicate<TargetEntry>> getTargetResultFilter(String user) {
		return getList(user, UserSetting::getTargetResultFilter);
	}

	@Override
	public List<Predicate<String>> getLabelValueFilter(String user, String labelName) {
		return Optional.ofNullable(this.userSettings.get(user)).map(UserSetting::getLabelValueFilter)
				.map(x -> x.get(labelName)).map(this::createFilter).orElseGet(Collections::emptyList);
	}

	@Override
	public List<Predicate<TargetMetaDataEntry>> getTargetMetaDataFilter(String user) {
		return getList(user, UserSetting::getTargetMetaDataResultFilter);
	}

	@Override
	public List<Predicate<MetaDataEntry>> getMetaDataFilter(String user) {
		return Optional.ofNullable(this.userSettings.get(user)).map(UserSetting::getMetaDataResultFilter)
				.map(this::createFilter).orElseGet(Collections::emptyList);
	}

	@Override
	public void checkQueryPermission(String user, boolean rangeQuery) {
		checkPermission(user, UserResourceAccessPermission::isQueryAccessAllowed);

	}

	@Override
	public void checkSeriesPermission(String user) {
		checkPermission(user, UserResourceAccessPermission::isSeriesAccessAllowed);
	}

	@Override
	public void checkLabelsPermission(String user) {
		checkPermission(user, UserResourceAccessPermission::isLabelsAccessAllowed);
	}

	@Override
	public void checkLabelValuesPermission(String user) {
		checkPermission(user, UserResourceAccessPermission::isLabelValuesAccessAllowed);
	}

	@Override
	public void checkTargetsPermission(String user) {
		checkPermission(user, UserResourceAccessPermission::isTargetsAccessAllowed);
	}

	@Override
	public void checkRulesPermission(String user) {
		checkPermission(user, UserResourceAccessPermission::isRulesAccessAllowed);
	}

	@Override
	public void checkAlertsPermission(String user) {
		checkPermission(user, UserResourceAccessPermission::isAlertsAccessAllowed);
	}

	@Override
	public void checkTargetMetaDataPermission(String user) {
		checkPermission(user, UserResourceAccessPermission::isTargetMetaDataAccessAllowed);
	}

	@Override
	public void checkMetaDataPermission(String user) {
		checkPermission(user, UserResourceAccessPermission::isMetaDataAccessAllowed);
	}

	private void checkPermission(String user, Function<UserResourceAccessPermission, Boolean> getter) {
		Optional.ofNullable(this.userSettings.get(user)).map(UserSetting::getResourceAccessPermissions).map(getter)
				.filter(Boolean::booleanValue).orElseThrow(PermissionDeniedException::new);
	}

	private <T> List<T> getList(String user, Function<UserSetting, List<T>> getter) {
		return Optional.ofNullable(this.userSettings.get(user)).map(getter).orElseGet(Collections::emptyList);
	}

	private <T> List<Predicate<T>> createFilter(List<FilterProducer<T>> producer) {
		return producer.stream().map(x -> x.createFilter(filterCtx)).collect(Collectors.toList());
	}

}
