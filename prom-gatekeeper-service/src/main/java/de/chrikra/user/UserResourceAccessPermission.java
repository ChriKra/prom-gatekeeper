package de.chrikra.user;

import lombok.Data;

/**
 * Permission settings for resource (REST-API) access
 * 
 * @author chrikra
 *
 */
@Data
public class UserResourceAccessPermission {
	private boolean queryAccessAllowed, seriesAccessAllowed, labelsAccessAllowed, labelValuesAccessAllowed,
			targetsAccessAllowed, rulesAccessAllowed, alertsAccessAllowed, targetMetaDataAccessAllowed,
			metaDataAccessAllowed;
}
