package de.chrikra;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import de.chrikra.dto.TargetMetaDataEntry;
import de.chrikra.dto.TargetMetaDataResult;
import de.chrikra.dto.TargetMetaDataTarget;
import io.quarkus.scheduler.Scheduled;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationScoped
public class MetaDataCache {

	@Inject
	@RestClient
	PrometheusClient client;

	private Map<String, Set<String>> metricsByInstance;

	private Map<String, Set<String>> metricsByJob;
	
	private Map<String, String> jobsByInstances;
	
	private Map<String, Set<String>> instancesByJobs;

	public Set<String> getMetricForInstance(String instance) {
		return metricsByInstance.getOrDefault(instance, Collections.emptySet());
	}
	
	public Set<String> getMetricsForInstances(List<String> instances) {
		return getValuesForKeys(instances, metricsByInstance);
	}
	
	public Set<String> getMetricForJob(String job) {
		return metricsByJob.getOrDefault(job, Collections.emptySet());
	}
	
	public Set<String> getMetricsForJobs(List<String> jobs) {
		return getValuesForKeys(jobs, metricsByJob);
	}
	
	private Set<String> getValuesForKeys(Collection<String> keys, Map<String, Set<String>> map){
		Set<String> result = new HashSet<String>();
		for(String key : keys) {
			Set<String> set = map.get(key);
			if(set!=null) {
				result.addAll(set);
			}
		}
		return result;
	}

	@Scheduled(every = "{promgatekeeper.metadata.cache.refresh.intervall}")
	public void updateData() {
		log.info("Updating meta data cache");
		Response resp = client.targetMetaData(null, null, null);
		if (resp.getStatus() != 200) {
			log.info("Can't get meta data from prometheus");
			return;
		}
		TargetMetaDataResult result = resp.readEntity(TargetMetaDataResult.class);
		List<TargetMetaDataEntry> data = result.getData();

		Map<String, Set<String>> metricsByInst = new HashMap<>();
		Map<String, Set<String>> metricsByJob = new HashMap<>();
		Map<String, String> jobsByInstances = new HashMap<>();
		Map<String, Set<String>> instancesByJobs = new HashMap<>();
		for (TargetMetaDataEntry e : data) {
			TargetMetaDataTarget target = e.getTarget();
			String instance = target.getInstance();
			Set<String> instanceMetrics = metricsByInst.get(instance);
			if (instanceMetrics == null) {
				instanceMetrics = new HashSet<>();
				metricsByInst.put(instance, instanceMetrics);
			}
			instanceMetrics.add(e.getMetric());
			
			String job = target.getJob();
			Set<String> jobMetrics = metricsByJob.get(job);
			if (jobMetrics == null) {
				jobMetrics = new HashSet<>();
				metricsByJob.put(job, jobMetrics);
			}
			jobMetrics.add(e.getMetric());
			jobsByInstances.put(instance, job);
			Set<String> instances = instancesByJobs.get(job);
			if(instances == null) {
				instances = new HashSet<>();
				instancesByJobs.put(job, instances);
			}
			instances.add(instance);
		}
		this.metricsByInstance = metricsByInst;
		this.metricsByJob = metricsByJob;
		this.instancesByJobs = instancesByJobs;
		this.jobsByInstances = jobsByInstances;
	}

	public Collection<String> getJobsForInstances(List<String> allowedInstances) {
		Set<String> result = new HashSet<String>();
		for(String instance : allowedInstances) {
			String job = this.jobsByInstances.get(instance);
			if(job!=null) {
				result.add(job);
			}
		}
		return result;
	}

	public Collection<String> getInstancesForJobs(List<String> allowedJobs) {
		return getValuesForKeys(allowedJobs, instancesByJobs);
	}
}
