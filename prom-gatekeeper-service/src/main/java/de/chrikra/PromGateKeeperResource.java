package de.chrikra;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import de.chrikra.user.UserService;
import io.quarkus.security.identity.SecurityIdentity;
import lombok.extern.slf4j.Slf4j;

@Path("/api/v1")
@Slf4j
public class PromGateKeeperResource implements PrometheusServiceDefinition {

	@Inject
	PromGateKeeperService service;

	@Inject
	SecurityIdentity identity;

	@Inject
	UserService userService;

	@Override
	public Response query(String query, String time, String timeout) {
		log.debug("Calling query '{}', with time '{}' and timeout '{}'", query, time, timeout);
		String user = getUser();
		userService.checkQueryPermission(user, false);
		return this.service.query(query, time, timeout, false, user);
	}

	@Override
	public Response queryPost(String query, String time, String timeout) {
		log.debug("Calling query (post) '{}', with time '{}' and timeout '{}'", query, time, timeout);
		String user = getUser();
		userService.checkQueryPermission(user, false);
		return this.service.query(query, time, timeout, true, user);
	}

	@Override
	public Response queryRange(String query, String start, String end, String step, String timeout) {
		log.debug("Calling query '{}', with start '{}',end '{}', step '{}' and timeout '{}'", query, start, end, step,
				timeout);
		String user = getUser();
		userService.checkQueryPermission(user, true);
		return this.service.queryRange(query, start, end, step, timeout, false, user);
	}

	@Override
	public Response queryRangePost(String query, String start, String end, String step, String timeout) {
		log.debug("Calling query (post) '{}', with start '{}',end '{}', step '{}' and timeout '{}'", query, start, end,
				step, timeout);
		String user = getUser();
		userService.checkQueryPermission(user, true);
		return this.service.queryRange(query, start, end, step, timeout, true, user);
	}

	@Override
	public Response series(String match, String start, String end) {
		log.debug("Calling series with match '{}', start '{}' and end '{}'", match, start, end);
		String user = getUser();
		userService.checkSeriesPermission(user);
		return this.service.series(match, start, end, false, user);
	}

	@Override
	public Response seriesPost(String match, String start, String end) {
		log.debug("Calling series (post) with match '{}', start '{}' and end '{}'", match, start, end);
		String user = getUser();
		userService.checkSeriesPermission(user);
		return this.service.series(match, start, end, true, user);
	}

	@Override
	public Response labels() {
		log.debug("Calling labels");
		String user = getUser();
		userService.checkLabelsPermission(user);
		return this.service.labels(false);
	}

	@Override
	public Response labelsPost() {
		log.debug("Calling labels (post)");
		String user = getUser();
		userService.checkLabelsPermission(user);
		return this.service.labels(true);
	}

	@Override
	public Response labelValues(String labelName) {
		log.debug("Calling labelValues with labelName '{}'", labelName);
		String user = getUser();
		userService.checkLabelValuesPermission(user);
		return this.service.labelValues(labelName, user);
	}

	@Override
	public Response targets(String state) {
		log.debug("Calling targets with state '{}'", state);
		String user = getUser();
		userService.checkTargetsPermission(user);
		return this.service.targets(state, user);
	}

	@Override
	public Response rules(String type) {
		log.debug("Calling rules with type '{}'", type);
		String user = getUser();
		userService.checkRulesPermission(user);
		return this.service.rules(type);
	}

	@Override
	public Response alerts() {
		log.debug("Calling alerts");
		String user = getUser();
		userService.checkAlertsPermission(user);
		return this.service.alerts();
	}

	@Override
	public Response targetMetaData(String matchTarget, String metric, Integer limit) {
		log.debug("Calling targetMetaData with matchTarget '{}', metric '{}' and limit '{}'", matchTarget, metric,
				limit);
		String user = getUser();
		userService.checkTargetMetaDataPermission(user);
		return this.service.targetMetaData(matchTarget, metric, limit, user);
	}

	@Override
	public Response metaData(Integer limit, String metric) {
		log.debug("Calling metaData with metric '{}' and limit '{}'", metric,
				limit);
		String user = getUser();
		userService.checkMetaDataPermission(user);
		return this.service.metaData(metric, limit, user);
	}
	
	private String getUser() {
		log.debug("User: {}", this.identity.getPrincipal());
		return this.identity.isAnonymous() ? "anon" : identity.getPrincipal().getName();
	}



}