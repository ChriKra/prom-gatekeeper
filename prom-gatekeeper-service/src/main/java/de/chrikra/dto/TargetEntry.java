package de.chrikra.dto;

import java.util.Date;
import java.util.Map;

import lombok.Data;

@Data
public class TargetEntry {
	private Map<String, String> discoveredLabels;
	private Map<String, String> labels;
	private String scrapePool;
	private String scrapeUrl;
	private String lastError;
	private Date lastScrape;
	private double lastScrapeDuration;
	private String health;
}
