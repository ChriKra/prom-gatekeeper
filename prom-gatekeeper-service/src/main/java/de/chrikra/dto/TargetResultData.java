package de.chrikra.dto;

import java.util.List;

import lombok.Data;

@Data
public class TargetResultData {
	private List<TargetEntry> activeTargets;
	private List<TargetEntry> droppedTargets;
}
