package de.chrikra.dto;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import de.chrikra.json.MetaDataResultListDeserializer;
import de.chrikra.json.MetaDataResultListSerializer;
import lombok.Data;

@JsonDeserialize(using = MetaDataResultListDeserializer.class)
@JsonSerialize(using = MetaDataResultListSerializer.class)
@Data
public class MetaDataResultList {
	private List<MetaDataEntry> data;
}
