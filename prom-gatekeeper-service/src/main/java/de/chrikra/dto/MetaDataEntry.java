package de.chrikra.dto;

import java.util.List;

import lombok.Data;

@Data
public class MetaDataEntry {
	private String metric;
	private List<MetricDescription> descriptions;
}
