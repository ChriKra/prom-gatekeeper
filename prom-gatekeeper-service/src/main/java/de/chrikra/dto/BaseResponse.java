package de.chrikra.dto;

import java.util.List;

import lombok.Data;

@Data
public class BaseResponse<T>{
	private String status;
	private T data;
	private String errorType;
	private String error;
	private List<String> warnings;
}
