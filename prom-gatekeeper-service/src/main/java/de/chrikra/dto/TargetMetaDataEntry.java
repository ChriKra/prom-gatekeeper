package de.chrikra.dto;

import lombok.Data;

@Data
public class TargetMetaDataEntry {
	private TargetMetaDataTarget target;
	private String metric, type, help, unit;
}
