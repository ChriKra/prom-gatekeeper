package de.chrikra.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SeriesResultEntry {
	@JsonProperty("__name__")
	private String name;
	private String instance;
	private String job;
	
}
