package de.chrikra.dto;

import lombok.Data;

@Data
public class MetricDescription {
	private String type, help, unit;
}
