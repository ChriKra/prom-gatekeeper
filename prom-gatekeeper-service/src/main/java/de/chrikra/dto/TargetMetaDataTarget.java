package de.chrikra.dto;

import lombok.Data;

@Data
public class TargetMetaDataTarget {
	private String instance, job;
}
