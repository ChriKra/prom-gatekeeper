package de.chrikra.query.modifier;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import de.chrikra.query.PromQLParser;
import de.chrikra.query.QueryElement;

public class RestrictLabelQueryModifierTest {

	@Test
	public void testSimpleQuery() throws IOException, QueryModificationException {
		String modifiedQuery = modify("http_requests", "instance", "localhost");
		assertThat(modifiedQuery, equalTo("http_requests{instance=\"localhost\"}"));
	}
	
	@Test
	public void testSimpleQueryWithLabel() throws IOException, QueryModificationException {
		String modifiedQuery = modify("http_requests{job=\"xxx\"}", "instance", "localhost");
		assertThat(modifiedQuery, equalTo("http_requests{job=\"xxx\",instance=\"localhost\"}"));
	}
	
	@Test
	public void testSimpleQueryWithAllowedLabel() throws IOException, QueryModificationException {
		String modifiedQuery = modify("http_requests{instance=\"localhost\"}", "instance", "localhost");
		assertThat(modifiedQuery, equalTo("http_requests{instance=\"localhost\"}"));
	}
	
	@Test
	public void testSimpleQueryWithDeniedLabel() throws IOException {
		try {
			modify("http_requests{instance=\"12.12.12.12\"}", "instance", "localhost");
			fail("An exception should have been thrown!");
		} catch (QueryModificationException e) {
			assertThat(e, instanceOf(EmptyResultException.class));
			EmptyResultException ere = (EmptyResultException)e;
			assertThat(ere.getResultType(), equalTo("vector"));
		}
	}
	
	@Test
	public void testFunctionCall() throws IOException, QueryModificationException {
		String modifiedQuery = modify("avg(http_requests)", "instance", "localhost");
		assertThat(modifiedQuery, equalTo("avg(http_requests{instance=\"localhost\"})"));
	}
	
	@Test
	public void testFunctionCallWithLabel() throws IOException, QueryModificationException {
		String modifiedQuery = modify("avg(http_requests{job=\"xxx\"})", "instance", "localhost");
		assertThat(modifiedQuery, equalTo("avg(http_requests{job=\"xxx\",instance=\"localhost\"})"));
	}
	
	@Test
	public void testFunctionCallWithAllowedLabel() throws IOException, QueryModificationException {
		String modifiedQuery = modify("avg(http_requests{instance=\"localhost\"})", "instance", "localhost");
		assertThat(modifiedQuery, equalTo("avg(http_requests{instance=\"localhost\"})"));
	}
	
	@Test
	public void testFunctionCallWithDeniedLabel() throws IOException {
		try {
			modify("avg(http_requests{instance=\"12.12.12.12\"})", "instance", "localhost");
			fail("An exception should have been thrown!");
		} catch (QueryModificationException e) {
			assertThat(e, instanceOf(EmptyResultException.class));
			EmptyResultException ere = (EmptyResultException)e;
			assertThat(ere.getResultType(), equalTo("vector"));
		}
	}

	private String modify(String query, String label, String... allowedValues)
			throws IOException, QueryModificationException {
		QueryElement parsedQuery = parse(query);
		parsedQuery.apply(new RestrictLabelQueryModifier(label, Arrays.asList(allowedValues)));
		return parsedQuery.toString();

	}

	private QueryElement parse(String string) throws IOException {
		return new PromQLParser().parse(string);
	}
}
