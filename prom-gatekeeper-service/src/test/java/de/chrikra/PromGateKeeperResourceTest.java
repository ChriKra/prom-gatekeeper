package de.chrikra;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.security.Principal;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import static org.junit.jupiter.api.Assertions.*;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.chrikra.user.UserService;
import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;

@QuarkusTest
public class PromGateKeeperResourceTest {
	private static final String TEST_USER = "testUser";

	@Inject
	PromGateKeeperResource testee;

	@InjectMock
	SecurityIdentity identity;

	@InjectMock
	UserService userService;

	@InjectMock
	PromGateKeeperService service;

	//Inject a mock for MetaDataCache to disable the background tasks
	@InjectMock
	MetaDataCache cache;
	
	private Response expectedResponse;

	@BeforeEach
	public void setup() {
		Principal p = mock(Principal.class);
		when(p.getName()).thenReturn(TEST_USER);
		when(identity.getPrincipal()).thenReturn(p);
		expectedResponse = Response.ok().build();
	}

	@Test
	public void testQueryGet() {
		String query = "some query";
		String time = "some time";
		String timeout = "some timeout";

		when(this.service.query(query, time, timeout, false, TEST_USER)).thenReturn(expectedResponse);

		Response resp = testee.query(query, time, timeout);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkQueryPermission(TEST_USER, false);
		verify(this.service).query(query, time, timeout, false, TEST_USER);
	}

	@Test
	public void testQueryPost() {
		String query = "some query";
		String time = "some time";
		String timeout = "some timeout";

		when(this.service.query(query, time, timeout, true, TEST_USER)).thenReturn(expectedResponse);

		Response resp = testee.queryPost(query, time, timeout);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkQueryPermission(TEST_USER, false);
		verify(this.service).query(query, time, timeout, true, TEST_USER);
	}

	@Test
	public void testQueryRangeGet() {
		String query = "some query";
		String timeout = "some timeout";
		String start = "start";
		String end = "end";
		String step = "step";

		when(this.service.queryRange(query, start, end, step, timeout, false, TEST_USER)).thenReturn(expectedResponse);

		Response resp = testee.queryRange(query, start, end, step, timeout);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkQueryPermission(TEST_USER, true);
		verify(this.service).queryRange(query, start, end, step, timeout, false, TEST_USER);
	}

	@Test
	public void testQueryRangePost() {
		String query = "some query";
		String timeout = "some timeout";
		String start = "start";
		String end = "end";
		String step = "step";

		when(this.service.queryRange(query, start, end, step, timeout, true, TEST_USER)).thenReturn(expectedResponse);

		Response resp = testee.queryRangePost(query, start, end, step, timeout);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkQueryPermission(TEST_USER, true);
		verify(this.service).queryRange(query, start, end, step, timeout, true, TEST_USER);
	}

	@Test
	public void testSeriesGet() {
		String match = "match";
		String start = "start";
		String end = "end";

		when(this.service.series(match, start, end, false, TEST_USER)).thenReturn(expectedResponse);

		Response resp = testee.series(match, start, end);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkSeriesPermission(TEST_USER);
		verify(this.service).series(match, start, end, false, TEST_USER);
	}

	@Test
	public void testSeriesPost() {
		String match = "match";
		String start = "start";
		String end = "end";

		when(this.service.series(match, start, end, true, TEST_USER)).thenReturn(expectedResponse);

		Response resp = testee.seriesPost(match, start, end);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkSeriesPermission(TEST_USER);
		verify(this.service).series(match, start, end, true, TEST_USER);
	}

	@Test
	public void testLabelsGet() {
		when(this.service.labels(false)).thenReturn(expectedResponse);

		Response resp = testee.labels();
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkLabelsPermission(TEST_USER);
		verify(this.service).labels(false);
	}

	@Test
	public void testLabelsPost() {
		when(this.service.labels(true)).thenReturn(expectedResponse);

		Response resp = testee.labelsPost();
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkLabelsPermission(TEST_USER);
		verify(this.service).labels(true);
	}

	@Test
	public void testLabelValues() {
		String label = "some label";
		when(this.service.labelValues(label, TEST_USER)).thenReturn(expectedResponse);

		Response resp = testee.labelValues(label);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkLabelValuesPermission(TEST_USER);
		verify(this.service).labelValues(label, TEST_USER);
	}

	@Test
	public void testTargets() {
		String state = "some state";
		when(this.service.targets(state, TEST_USER)).thenReturn(expectedResponse);

		Response resp = testee.targets(state);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkTargetsPermission(TEST_USER);
		verify(this.service).targets(state, TEST_USER);
	}

	@Test
	public void testRules() {
		String type = "some rule type";
		when(this.service.rules(type)).thenReturn(expectedResponse);

		Response resp = testee.rules(type);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkRulesPermission(TEST_USER);
		verify(this.service).rules(type);
	}

	@Test
	public void testAlerts() {
		when(this.service.alerts()).thenReturn(expectedResponse);

		Response resp = testee.alerts();
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkAlertsPermission(TEST_USER);
		verify(this.service).alerts();
	}

	@Test
	public void testTargetMetaData() {
		String matchTarget = "matchTarget";
		String metric = "metric";
		Integer limit = 12;
		
		when(this.service.targetMetaData(matchTarget, metric, limit, TEST_USER)).thenReturn(expectedResponse);

		Response resp = testee.targetMetaData(matchTarget, metric, limit);
		assertEquals(resp, expectedResponse);

		verify(this.userService).checkTargetMetaDataPermission(TEST_USER);;
		verify(this.service).targetMetaData(matchTarget, metric, limit, TEST_USER);
	}

}
