package de.chrikra;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import de.chrikra.query.modifier.QueryModificationException;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;

@QuarkusTest
public class PromGateKeeperServiceTest {
	private static final String TEST_QUERY = "SomeMetric";
	private static final String MODIFIED_QUERY = "SomeMetric{instance=\"localhost\"}";
	private static final String USER = "anon";
	private static final Response PROMETHEUS_RESPONSE = Response.ok().build();

	@Inject
	PromGateKeeperService testee;

	@InjectMock
	@RestClient
	PrometheusClient prometheusClient;

	@InjectMock
	QueryModifierService queryModifier;

	@InjectMock
	ResultFilterService resultFilter;

	@BeforeEach
	public void setup() throws QueryModificationException, IOException {
		when(queryModifier.modifyQuery(TEST_QUERY, USER)).thenReturn(MODIFIED_QUERY);
		when(this.prometheusClient.targetMetaData(null, null, null)).thenReturn(Response.status(Status.BAD_REQUEST).build());
	}

	@Test
	void testQueryGet() throws QueryModificationException, IOException {
		String time = "5m";
		String timeout = "1m";

		when(prometheusClient.query(MODIFIED_QUERY, time, timeout)).thenReturn(PROMETHEUS_RESPONSE);

		Response resp = testee.query(TEST_QUERY, time, timeout, false, USER);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.queryModifier).modifyQuery(TEST_QUERY, USER);
		verify(this.prometheusClient).query(MODIFIED_QUERY, time, timeout);
	}

	@Test
	void testQueryPost() throws QueryModificationException, IOException {
		String time = "5m";
		String timeout = "1m";

		when(prometheusClient.queryPost(MODIFIED_QUERY, time, timeout)).thenReturn(PROMETHEUS_RESPONSE);

		Response resp = testee.query(TEST_QUERY, time, timeout, true, USER);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.queryModifier).modifyQuery(TEST_QUERY, USER);
		verify(this.prometheusClient).queryPost(MODIFIED_QUERY, time, timeout);
	}

	@Test
	void testQueryRangeGet() throws QueryModificationException, IOException {
		String start = "start";
		String end = "end";
		String step = "step";
		String timeout = "timeout";
		when(prometheusClient.queryRange(MODIFIED_QUERY, start, end, step, timeout)).thenReturn(PROMETHEUS_RESPONSE);

		Response resp = testee.queryRange(TEST_QUERY, start, end, step, timeout, false, USER);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.queryModifier).modifyQuery(TEST_QUERY, USER);
		verify(this.prometheusClient).queryRange(MODIFIED_QUERY, start, end, step, timeout);
	}

	@Test
	void testQueryRangePost() throws QueryModificationException, IOException {
		String start = "start";
		String end = "end";
		String step = "step";
		String timeout = "timeout";
		when(prometheusClient.queryRangePost(MODIFIED_QUERY, start, end, step, timeout))
				.thenReturn(PROMETHEUS_RESPONSE);

		Response resp = testee.queryRange(TEST_QUERY, start, end, step, timeout, true, USER);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.queryModifier).modifyQuery(TEST_QUERY, USER);
		verify(this.prometheusClient).queryRangePost(MODIFIED_QUERY, start, end, step, timeout);
	}

	@Test
	void testSeriesGet() {
		String match = "match";
		String start = "start";
		String end = "end";

		Response tmpResult = Response.accepted().build();
		when(this.prometheusClient.series(match, start, end)).thenReturn(tmpResult);
		when(this.resultFilter.filterSeriesResult(tmpResult, USER)).thenReturn(PROMETHEUS_RESPONSE);

		Response resp = this.testee.series(match, start, end, false, USER);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.prometheusClient).series(match, start, end);
		verify(this.resultFilter).filterSeriesResult(tmpResult, USER);
	}

	@Test
	void testSeriesPost() {
		String match = "match";
		String start = "start";
		String end = "end";

		Response tmpResult = Response.accepted().build();
		when(this.prometheusClient.seriesPost(match, start, end)).thenReturn(tmpResult);
		when(this.resultFilter.filterSeriesResult(tmpResult, USER)).thenReturn(PROMETHEUS_RESPONSE);

		Response resp = this.testee.series(match, start, end, true, USER);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.prometheusClient).seriesPost(match, start, end);
		verify(this.resultFilter).filterSeriesResult(tmpResult, USER);
	}

	@Test
	void testLabels() {
		when(this.prometheusClient.labels()).thenReturn(PROMETHEUS_RESPONSE);
		Response resp = this.testee.labels(false);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.prometheusClient).labels();
	}

	@Test
	void testLabelsPost() {
		when(this.prometheusClient.labelsPost()).thenReturn(PROMETHEUS_RESPONSE);
		Response resp = this.testee.labels(true);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.prometheusClient).labelsPost();
	}

	@Test
	void testLabelValues() {
		String labelName = "label";
		Response tmpResult = Response.accepted().build();
		when(this.prometheusClient.labelValues(labelName)).thenReturn(tmpResult);
		when(this.resultFilter.filterLabelValues(tmpResult, labelName, USER)).thenReturn(PROMETHEUS_RESPONSE);
		Response resp = this.testee.labelValues(labelName, USER);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.prometheusClient).labelValues(labelName);
		verify(this.resultFilter).filterLabelValues(tmpResult, labelName, USER);
	}

	@Test
	void testTargets() {
		String state = "state";
		Response tmpResult = Response.accepted().build();
		when(this.resultFilter.filterTargetResults(tmpResult, USER)).thenReturn(PROMETHEUS_RESPONSE);
		when(this.prometheusClient.targets(state)).thenReturn(tmpResult);
		Response resp = this.testee.targets(state, USER);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.prometheusClient).targets(state);
		verify(this.resultFilter).filterTargetResults(tmpResult, USER);
	}

	@Test
	void testRules() {
		String type = "type";
		when(this.prometheusClient.rules(type)).thenReturn(PROMETHEUS_RESPONSE);
		Response resp = this.testee.rules(type);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.prometheusClient).rules(type);
	}

	@Test
	void testAlerts() {
		when(this.prometheusClient.alerts()).thenReturn(PROMETHEUS_RESPONSE);
		Response resp = this.testee.alerts();
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.prometheusClient).alerts();
	}

	@Test
	void testTargetMetaData() {
		Integer limit = 1;
		String matchTarget = "matchTarget";
		String metric = "metric";
		
		Response tmpResult = Response.accepted().build();
		when(this.prometheusClient.targetMetaData(matchTarget, metric, limit)).thenReturn(tmpResult);
		when(this.resultFilter.filterTargetMetaDataResult(tmpResult , USER)).thenReturn(PROMETHEUS_RESPONSE);
		
		Response resp = this.testee.targetMetaData(matchTarget , metric , limit , USER);
		assertEquals(PROMETHEUS_RESPONSE, resp);
		verify(this.prometheusClient).targetMetaData(matchTarget, metric, limit);
		verify(this.resultFilter).filterTargetMetaDataResult(tmpResult , USER);
	}

}
